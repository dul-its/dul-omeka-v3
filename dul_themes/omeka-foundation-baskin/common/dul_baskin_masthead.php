<header role="banner" class="l-header">

	<div class="masthead-wrapper">
    <div class="grid-x">
      <div class="small-12">
        <div class="grid-container masthead">

          <div class="library-logo">
            <a href="//library.duke.edu" title="Duke University Libraries"><img src="/themes/omeka-foundation-dul/images/dul/dul_logo.png" alt="Duke University Libraries"></a>
          </div>

          <div class="exhibits-logo">
            <a href="//library.duke.edu/exhibits" title="Library Exhibits"><img src="/themes/omeka-foundation-dul/images/dul/exhibits_logo.png" alt="Exhibits"></a>
          </div>
        
        </div>
      </div>
    </div>
	</div>


	<div class="nav-wrapper">
		<div class="grid-x">
			<div class="small-12">
				<section class="top-bar-section text-center" id="nav">

					<?php
						// get exhibit homepage link
						$exhibitSlug = 'baskin';
						$exhibitHomeURI = html_escape(WEB_ROOT) . '/exhibits/show/' . $exhibitSlug;
						?>

						<div class="baskin-logo <?php echo $logo_class; ?>">
							<a href="<?php echo $exhibitHomeURI ?>">
								<img src="<?php echo img($logo_file); ?>" alt="Five Hundred Years of Women's Work: The Lisa Unger Baskin Collection" />
							</a>
						</div>

					<div class="title-bar" data-responsive-toggle="baskin-menu" data-hide-for="large">
						<button type="button" data-toggle="baskin-menu">Menu</button>
					</div>
					
					<nav class="top-bar grid-container" id="baskin-menu" data-topbar="" role="navigation" aria-label="Main menu">
					    <ul class="links vertical medium-horizontal menu dropdown" data-dropdown-menu>
								

							<li><a href="/exhibits/show/baskin/introduction">Introduction</a></li>

							<li>
								<a href="/exhibits/show/baskin/about">About</a>
								<ul class="menu">
									<li><a href="/exhibits/show/baskin/about/collectors-statement">Collector's Statement</a></li>
									<li><a href="/exhibits/show/baskin/about/interview-with-lisa-baskin">Interview with Lisa Baskin</a></li>
									<li><a href="/exhibits/show/baskin/about/curator-biographies">Curator Biographies</a></li>
									<li><a href="/exhibits/show/baskin/about/acknowledgements-and-sponsors">Acknowledgements and Sponsors</a></li>
									<li><a href="/exhibits/show/baskin/about/exhibition-catalogue-available">Exhibition Catalogue Available</a></li>
								</ul>
							</li>

							<li>
								<a href="/exhibits/show/baskin/essays">Essays</a>
								<ul class="menu">
									<li><a href="/exhibits/show/baskin/essays/forward">Forward</a></li>
									<li><a href="/exhibits/show/baskin/essays/insatiable-lust">Insatiable Lust</a></li>
									<li><a href="/exhibits/show/baskin/essays/never-done">Never Done</a></li>
									<li><a href="/exhibits/show/baskin/essays/the-future-is-female">The Future is Female and So Was the Past</a></li>
								</ul>
							</li>

							<li>
								<a href="/exhibits/show/baskin/events">Events</a>
								<ul class="menu">
									<li><a href="/exhibits/show/baskin/events/duke-opening">Duke Exhibit Opening Program</a></li>
									<li><a href="/exhibits/show/baskin/events/symposium">Symposium on Women Across the Disciplines</a></li>
									<li><a href="/exhibits/show/baskin/events/book-arts-symposium">Symposium on Women in the Book Arts</a></li>	
									<li><a href="/exhibits/show/baskin/events/gallery-tours">Gallery Tours with Lisa Unger Baskin</a></li>
									<li><a href="/exhibits/show/baskin/events/grolier-club-lecture">Grolier Club Lecture by Lisa Unger Baskin</a></li>
								</ul>
							</li>

							<li>
								<a href="/exhibits/show/baskin/explore">Explore</a>
								<ul class="menu">
									<li><a href="/exhibits/show/baskin/explore/1200s-1500s">1200s &ndash; 1500s</a></li>
									<li><a href="/exhibits/show/baskin/explore/1600s">1600s</a></li>
									<li><a href="/exhibits/show/baskin/explore/1700s">1700s</a></li>
									<li><a href="/exhibits/show/baskin/explore/1800s">1800s</a></li>
									<li><a href="/exhibits/show/baskin/explore/1900s">1900s</a></li>
									<li><a href="/exhibits/show/baskin/explore/bindings">Bindings</a></li>
									<li><a href="/exhibits/show/baskin/explore/trades">Trades</a></li>
								</ul>
							</li>


							</ul>
					</nav>

				</section>
			</div>
		</div>
	</div>

</header>
