<?php
foreach ($items as $item) {
  if ($item['public'] == '1') {

    //print_r($item);
    //echo '<br /><br />+++<br /><br />';

    set_current_record('item', $item);
    echo '<div class="masonry-item">';
      echo '<div class="image-wrapper">';
        echo '<a href="' . exhibit_builder_exhibit_item_uri($item) . '">';
          echo item_image('fullsize');
        echo '</a>';
      echo '</div>';
      echo '<div class="text-wrapper">';
        echo '<a href="' . exhibit_builder_exhibit_item_uri($item) . '">';
          echo '<h3>' . item_type_elements($item)['Hover Text'] . '</h3>';
        echo '</a>';
      echo '</div>';
    echo '</div>';

  }
}
?>
