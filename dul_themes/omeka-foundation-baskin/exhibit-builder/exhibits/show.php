<?php
// this gets url slug
// echo basename(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));


$_SERVER['REQUEST_URI_PATH'] = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$URL_SEGMENTS = explode('/', rtrim($_SERVER['REQUEST_URI_PATH'], '/'));
$URL_SEGMENTS = array_reverse($URL_SEGMENTS);


echo head(array(
    'title' => metadata('exhibit_page', 'title') . ' &middot; ' . metadata('exhibit', 'title'),
    'bodyclass' => 'exhibits show'));
?>

<div class="grid-x grid-margin-x">

    <div id="exhibit-content" class="cell large-12">

        <div id="exhibit-blocks">

        <?php exhibit_builder_render_exhibit_page(); ?>

        <?php

          // 1200s - 1500s
          if ($URL_SEGMENTS[0] == '1200s-1500s' && $URL_SEGMENTS[2] == 'baskin') {

            $items = get_records('Item',array('tags'=>'1200s to 1500s'),0);

            ?>
            <div id="masonry-container" class="small-block-grid-1 medium-block-grid-2 large-block-grid-3">
              <?php include('render_items.php'); ?>
            </div>

        <?php
          }
        ?>


        <?php

          // 1600s
          if ($URL_SEGMENTS[0] == '1600s' && $URL_SEGMENTS[2] == 'baskin') {
            $items = get_records('Item',array('tags'=>'1600s'),0);

            ?>
            <div id="masonry-container" class="small-block-grid-1 medium-block-grid-2 large-block-grid-3">
              <?php include('render_items.php'); ?>
            </div>

        <?php
          }
        ?>

        <?php

          // 1700s
          if ($URL_SEGMENTS[0] == '1700s' && $URL_SEGMENTS[2] == 'baskin') {
            $items = get_records('Item',array('tags'=>'1700s'),0);

            ?>
            <div id="masonry-container" class="small-block-grid-1 medium-block-grid-2 large-block-grid-3">
              <?php include('render_items.php'); ?>
            </div>

        <?php
          }
        ?>


        <?php

          // 1800s
          if ($URL_SEGMENTS[0] == '1800s' && $URL_SEGMENTS[2] == 'baskin') {
            $items = get_records('Item',array('tags'=>'1800s'),0);

            ?>
            <div id="masonry-container" class="small-block-grid-1 medium-block-grid-2 large-block-grid-3">
              <?php include('render_items.php'); ?>
            </div>

        <?php
          }
        ?>


        <?php

          // 1900s
          if ($URL_SEGMENTS[0] == '1900s' && $URL_SEGMENTS[2] == 'baskin') {
            $items = get_records('Item',array('tags'=>'1900s'),0);
            ?>

            <div id="masonry-container" class="small-block-grid-1 medium-block-grid-2 large-block-grid-3">
              <?php include('render_items.php'); ?>
            </div>

        <?php
          }
        ?>



        <?php

          // Book Bindings
          if ($URL_SEGMENTS[0] == 'bindings' && $URL_SEGMENTS[2] == 'baskin') {
            $items = get_records('Item',array('tags'=>'Bookbindings'),0);
            ?>

            <div id="masonry-container" class="small-block-grid-1 medium-block-grid-2 large-block-grid-3">
              <?php include('render_items.php'); ?>
            </div>

        <?php
          }
        ?>


        <?php

          // Trade Cards
          if ($URL_SEGMENTS[0] == 'trades' && $URL_SEGMENTS[2] == 'baskin') {
            $items = get_records('Item',array('tags'=>'Trades'),0);
            ?>

            <div id="masonry-container" class="small-block-grid-1 medium-block-grid-2 large-block-grid-3">
              <?php include('render_items.php'); ?>
            </div>

        <?php
          }
        ?>

        </div>

        <div class="grid-x grid-margin-x" id="exhibit-page-navigation">
            
          <div class="cell large-6 text-left">
            <?php if ($prevLink = exhibit_builder_link_to_previous_page()): ?>
              <?php echo $prevLink; ?>
            <?php endif; ?>
          </div>
          
          <div class="cell large-6 text-right">
            <?php if ($nextLink = exhibit_builder_link_to_next_page()): ?>
              <?php echo $nextLink; ?>
            <?php endif; ?>
          </div>

        </div>
            
        <div class="grid-x grid-margin-x item-navigation">
          <div class="large-12 cell text-center">
            <?php echo exhibit_builder_page_trail(); ?>
          </div>
        </div>

    </div>


</div>


<?php echo foot(); ?>
