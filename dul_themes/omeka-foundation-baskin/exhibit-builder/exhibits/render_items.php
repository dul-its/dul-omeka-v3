<?php

$itemsOrder = array();

foreach ($items as $item) {
  $myItemElements = item_type_elements($item);
  $myItemIndex = $myItemElements['Item Index'];
  $itemsOrder[$myItemIndex] = $item->id;
}

foreach ($itemsOrder as $itemID) {

  $item = get_record_by_id('item', $itemID);

  if ($item['public'] == '1') {
    
    set_current_record('item', $item);
   
    $myItemElements = item_type_elements($item);

    $titleText = metadata('item', array('Dublin Core', 'Title'));
    if ($myItemElement['Display Date']) {
      $titleText .= ', ' . $myItemElement['Display Date'];
    };

    if (isset($myItemElements['Alternative Text'])) {
        $alt = $myItemElements['Alternative Text'];
    } else {
        $alt = $titleText;
    }
?>

    <div class="masonry-item">

      <div class="image-wrapper">
        <a href="<?php echo exhibit_builder_exhibit_item_uri($item); ?>">
          <?php echo item_image('fullsize', array('alt' => $alt)); ?>
        </a>
      </div>

      <div class="text-wrapper">
        <h3>
          <a href="<?php echo exhibit_builder_exhibit_item_uri($item); ?>">
            <?php echo $myItemElements['Hover Text']; ?>
          </a>
        </h3>
        <p>
          <a href="<?php echo exhibit_builder_exhibit_item_uri($item); ?>">
          <?php echo $titleText; ?>
          </a>
        </p>
      </div>

    </div>

<?php
  }

}



?>
