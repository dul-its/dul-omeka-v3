<?php

/**
 * Exhibit gallery view helper.
 * 
 * @package ExhibitBuilder\View\Helper
 */
class ExhibitBuilder_View_Helper_exhibitAttachmentGalleryDUL extends Zend_View_Helper_Abstract {
    /**
     * Return the markup for a gallery of exhibit attachments.
     *
     * @uses ExhibitBuilder_View_Helper_ExhibitAttachment
     * @param ExhibitBlockAttachment[] $attachments
     * @param array $fileOptions
     * @param array $linkProps
     * @return string
     */
    
     // public function exhibitAttachmentGalleryDUL($attachments, $fileOptions = array(), $linkProps = array())
    // {

    //     $html = '';
    //     foreach  ($attachments as $attachment) {
    //         $html .= '<div class="exhibit-item exhibit-gallery-item">';
    //         $html .= $this->view->exhibitAttachmentDUL($attachment, array('imageSize' => 'square_thumbnail'));
    //         $html .= '</div>';
    //     }
    
    //     return apply_filters('exhibit_attachment_gallery_markup', $html,
    //         compact('attachments', 'fileOptions', 'linkProps'));
    // }

    public function exhibitAttachmentGalleryDUL($attachments, $fileOptions = array(), $linkProps = array())
    {
        // Set a default imageSize if not provided in $fileOptions
        $defaultImageSize = 'thumbnail';
        $imageSize = isset($fileOptions['imageSize']) ? $fileOptions['imageSize'] : $defaultImageSize;

        $html = '';
        foreach ($attachments as $attachment) {
            $html .= '<div class="exhibit-item exhibit-gallery-item">';
            $html .= $this->view->exhibitAttachmentDUL($attachment, array('imageSize' => $imageSize));
            $html .= '</div>';
        }

        return apply_filters('exhibit_attachment_gallery_markup', $html,
            compact('attachments', 'fileOptions', 'linkProps'));
    }
}