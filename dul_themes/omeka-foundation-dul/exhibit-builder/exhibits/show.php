<?php
echo head(array(
    'title' => metadata('exhibit_page', 'title') . ' &middot; ' . metadata('exhibit', 'title'),
    'bodyclass' => 'exhibits show'));
?>

<div class="grid-x grid-margin-x">

    <div id="exhibit-content" class="cell large-9 left">
          
        <h1><?php echo metadata('exhibit', 'title'); ?></span></h1>

        <h2><span class="exhibit-page"><?php echo metadata('exhibit_page', 'title'); ?></span></h2>
        
        <div id="exhibit-blocks">
            <?php exhibit_builder_render_exhibit_page(); ?>
        </div>
        
        <div id="exhibit-page-navigation">
            
        <?php if ($prevPage = $exhibit_page->previousOrParent()): ?>
            <div id="exhibit-nav-prev">
            <?php 
              $prevLabel = '← (previous page) ' . metadata($prevPage, 'menu_title');
              echo exhibit_builder_link_to_previous_page($prevLabel, array(
                  'class' => 'previous-page',
              ));
            ?>
            </div>
            <?php endif; ?>

            <?php if ($nextPage = $exhibit_page->firstChildOrNext()): ?>
            <div id="exhibit-nav-next">
            <?php 
              $nextLabel = '(next page) ' . metadata($nextPage, 'menu_title') . ' →';
              echo exhibit_builder_link_to_next_page($nextLabel, array(
                    'class' => 'next-page',
              )); 
            ?>
            </div>
            <?php endif; ?>

        </div>
    
    </div>

    <nav id="exhibit-pages" class="cell large-3">
        <div class="exhibit-tree-container">
            <?php echo exhibit_builder_page_tree($exhibit, $exhibit_page); ?>

            <?php if ($exhibitDescription = metadata('exhibit', 'description', array('no_escape' => true))): ?>
            <div class="exhibit-description">
                <?php echo $exhibitDescription; ?>
            </div>
            <?php endif; ?>
        </div>
    </nav>

</div>

<?php echo foot(); ?>
