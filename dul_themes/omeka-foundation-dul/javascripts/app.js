(function ($) {
  $(document).ready(function() {
      $('.search-toggle').click(function() {
        $('#search-container').toggleClass('open').toggleClass('closed');
      });

     $('.advanced-toggle').click(function() {
        $('#advanced-form').toggleClass('open');
        $(this).toggleClass('open');

        if ($(this).hasClass('open')) {
          $('#advanced-form input').first().focus();
        }
     });

       // hide pages for old 'book' layout block
      $('.exhibit-item').each(function() {
        if ($(this).find('.slide-meta').length > 0) {
            var captionContent = $(this).find('.exhibit-item-caption').html().trim();

            if (captionContent === '<p>&lt;empty&gt;</p>') {
                $(this).addClass('later-pages');
            }
        }
    });
  });
})(jQuery)