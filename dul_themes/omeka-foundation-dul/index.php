<?php echo head(array('bodyid' => 'home', 'redirect' => 'true')); ?>

  <h1>Duke Libraries Exhibits</h1>

  <p>This page will redirect to <a href="https://library.duke.edu/exhibits/online">https://library.duke.edu/exhibits/online</a></p>

  <?php fire_plugin_hook('public_home', array('view' => $this)); ?>
  
  <?php echo foot(); ?>
