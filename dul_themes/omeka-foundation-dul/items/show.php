<?php 
$layout = (get_theme_option('item_show_columns') !== null) ? get_theme_option('item_show_columns') : 'single';
$mediaPosition = (get_theme_option('media_position') !== null) ? get_theme_option('media_position') : 'top';
$mediaDisplay = get_theme_option('item_show_media_display');
$showLayout = get_theme_option('item_show_inline_metadata');

if ($mediaDisplay == 'lightgallery') {
    queue_lightgallery_assets();
}
echo head(array('title' => metadata('item', array('Dublin Core', 'Title')),'bodyclass' => 'resource items show ' . $layout)); 

if ($_GET['slug'] != ""):
    echo '<p><a href="' . htmlspecialchars($_GET['slug']) . '" title="Return to previous page">&laquo; Back to exhibit</a></p>';
endif;

?>

<div class="wrap">

    <h1><?php echo metadata('item', array('Dublin Core', 'Title')); ?></h1>
    
    <div class="grid-x grid-margin-x">

        <div class="cell large-6 left">
        
            <!-- Items metadata -->
            <div id="resource-values" class="<?php echo ($showLayout == 1) ? 'inline' : 'stack'; ?>">
                <?php echo all_element_texts('item'); ?>

            <?php fire_plugin_hook('public_items_show', array('view' => $this, 'item' => $item)); ?>
            </div>
        
        </div>
        
        <div class="cell large-6 right">
           
            <!-- The following returns all of the files associated with an item. -->
            <?php if (metadata('item', 'has files')): ?>
            <div class="cell large-12">
                <div id="itemfiles" class="element">
                    <div class="element-text"><?php echo files_for_item(
                        array('imageSize' => 'fullsize')
                    ); ?></div>
                </div>
            </div>
            <?php endif; ?>

        </div>

    </div>

</div> <!-- End of Primary. -->

<?php echo foot(); ?>
