<header role="banner" class="l-header">

	<div class="masthead-wrapper">
    <div class="grid-x">
      <div class="small-12">
        <div class="grid-container masthead">

          <div class="library-logo">
            <a href="//library.duke.edu" title="Duke University Libraries"><img src="/themes/omeka-foundation-dul/images/dul/dul_logo.png" alt="Duke University Libraries"></a>
          </div>

          <div class="exhibits-logo">
            <a href="//library.duke.edu/exhibits" title="Library Exhibits"><img src="/themes/omeka-foundation-dul/images/dul/exhibits_logo.png" alt="Exhibits"></a>
          </div>

          <div class="logo-spacer"><img src="/themes/omeka-foundation-dul/images/dul/mast_arrow.png" alt=""></div>
        
        </div>
      </div>
    </div>
	</div>


	<div class="nav-wrapper">
		<div class="grid-x">
			<div class="small-12">
				<section class="top-bar-section" id="nav">

				<div class="title-bar" data-responsive-toggle="exhibits-menu" data-hide-for="large">
					<button class="menu-icon" type="button" data-toggle="exhibits-menu"></button>
					<div class="title-bar-title">Menu</div>
				</div>
					
					<nav class="top-bar grid-container" id="exhibits-menu" data-topbar="" role="navigation" aria-label="Main menu">
					    <ul class="links vertical medium-horizontal menu">
								<li class="menu-item first"><a href="https://library.duke.edu/exhibits">Current Exhibits</a></li>
								<li class="menu-item"><a href="https://library.duke.edu/exhibits/past">Past</a></li>
								<li class="menu-item"><a href="https://library.duke.edu/exhibits/upcoming">Upcoming</a></li>
								<li class="menu-item active"><a href="https://library.duke.edu/exhibits/online" title="" class="active">Online</a></li>
								<li class="menu-item"><a href="https://library.duke.edu/exhibits/propose">Propose an Exhibit</a></li>
								<li class="menu-item"><a href="https://library.duke.edu/exhibits/student-exhibits" title="">About Student Exhibits</a></li>
								<li class="menu-item last"><a href="https://library.duke.edu/exhibits/about">About Gallery Spaces</a></li>
							</ul>
					</nav>

				</section>
			</div>
		</div>
	</div>

</header>
