<div class="row">
  <div id="harmful-language-statement" class="large-12 columns">
    <p><i class="fas fa-info-circle"></i> Some materials and descriptions may include offensive content. <strong><a href="https://library.duke.edu/about/statement-potentially-harmful-language-library-descriptions" aria-label="Statement on Potentially Harmful Language in Library Descriptions">More info</a></strong></p>
  </div>
</div>
