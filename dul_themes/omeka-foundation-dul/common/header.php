<!DOCTYPE html>
<html lang="<?php echo get_html_lang(); ?>">
<head>

<?php
if (@$bodyid == 'home' && @$redirect == 'true'): 
    echo '<meta http-equiv="refresh" content="0; url=https://library.duke.edu/exhibits/online">';
endif;
?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php if ($author = option('author')): ?>
    <meta name="author" content="<?php echo $author; ?>" />
    <?php endif; ?>
    <?php if ($copyright = option('copyright')): ?>
    <meta name="copyright" content="<?php echo $copyright; ?>" />
    <?php endif; ?>
    <?php if ( $description = option('description')): ?>
    <meta name="description" content="<?php echo $description; ?>" />
    <?php endif; ?>
    <?php
    if (isset($title)) {
        $titleParts[] = strip_formatting($title);
    }
    $titleParts[] = option('site_title');
    ?>
    <title><?php echo implode(' &middot; ', $titleParts); ?></title>

    <?php echo auto_discovery_link_tags(); ?>

    <!-- Plugin Stuff -->

    <?php fire_plugin_hook('public_head', array('view'=>$this)); ?>


    <!-- Stylesheets -->
    <?php
    $stylesheetOption = (get_theme_option('stylesheet')) ? get_theme_option('stylesheet') : 'default';
    $banner = get_theme_option('banner');
    $bannerWidth = (get_theme_option('banner_width')) ? get_theme_option('banner_width') : '';
    $bannerHeight = get_theme_option('banner_height_desktop');
    if ($bannerHeight == '') {
        $bannerHeight = 'auto';
    }
    $bannerHeightMobile = get_theme_option('banner_height_mobile');
    $bannerPosition = (get_theme_option('banner_position')) ? str_replace('_','-', get_theme_option('banner_position')) : 'center';
    queue_css_file(array('iconfonts'));
    queue_css_file($stylesheetOption);
    queue_css_string('
        .banner {
            height: ' .  $bannerHeight . ';
            background-position: ' . $bannerPosition . ';
        }'
    );
    if ($bannerHeightMobile !== '') {
        queue_css_string('
            @media screen and (max-width:640px) {
                .banner {
                    height: ' . $bannerHeightMobile . ';
                }
            }'
        );
    }
    queue_css_url('//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css');
    queue_css_file('lightbox');
    queue_css_file('dul');
    echo head_css();

    echo theme_header_background();
    ?>

    <!-- JavaScripts -->
    <?php
    queue_js_file(array('globals', 'app'));
    queue_js_url('//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/js/fontawesome.min.js');
    queue_js_url('//cdn.jsdelivr.net/npm/foundation-sites@6.7.5/dist/js/foundation.min.js', array(
      'integrity' => 'sha256-/PFxCnsMh+nTuM0k3VJCRch1gwnCfKjaP8rJNq5SoBg= sha384-9ksAFjQjZnpqt6VtpjMjlp2S0qrGbcwF/rvrLUg2vciMhwc1UJJeAAOLuJ96w+Nj sha512-UMSn6RHqqJeJcIfV1eS2tPKCjzaHkU/KqgAnQ7Nzn0mLicFxaVhm9vq7zG5+0LALt15j1ljlg8Fp9PT1VGNmDw==',
      'crossorigin' => 'anonymous',
    ));
    queue_js_file('lightbox.min','javascripts/vendor/lightbox/js');
    queue_js_file(array('globals', 'app'));
    echo head_js();
    ?>
</head>
<?php $navLayout = get_theme_option('navigation_layout'); ?>
<?php echo body_tag(array('id' => @$bodyid, 'class' => @$bodyclass . ' ' . $navLayout . '-menu')); ?>
    
<a href="#content" id="skipnav" class="skipnav"><?php echo __('Skip to main content'); ?></a>
    <a href="#exhibit-pages" id="skipnav_nav" class="skipnav"><?php echo __('Skip to navigation'); ?></a>
    
    <?php fire_plugin_hook('public_body', array('view'=>$this)); ?>
    
    <div id="offCanvas" class="off-canvas position-left" data-off-canvas>
        <?php echo use_foundation_navigation('vertical'); ?>
    </div>
    
    <div class="off-canvas-content" data-off-canvas-content>

    <?php include 'dul_masthead.php'; ?>
    
    <div id="content" role="main">
        <div class="grid-container">
        <?php fire_plugin_hook('public_content_top', array('view'=>$this)); ?>
