<header role="banner" class="l-header">

	<div class="masthead-wrapper">
    <div class="grid-x">
      <div class="small-12">
        <div class="grid-container masthead">

          <div class="library-logo">
            <a href="//library.duke.edu" title="Duke University Libraries"><img src="/themes/omeka-foundation-dul/images/dul/dul_logo.png" alt="Duke University Libraries"></a>
          </div>

          <div class="exhibits-logo">
            <a href="//library.duke.edu/exhibits" title="Library Exhibits"><img src="/themes/omeka-foundation-dul/images/dul/exhibits_logo.png" alt="Exhibits"></a>
          </div>
        
        </div>
      </div>
    </div>
	</div>


	<div class="nav-wrapper">
		<div class="grid-x">
			<div class="small-12">
				<section class="top-bar-section text-center" id="nav">

					<?php
						// get exhibit homepage link
						$exhibitSlug = 'ourduke';
						$exhibitHomeURI = html_escape(WEB_ROOT) . '/exhibits/show/' . $exhibitSlug;
						?>

						<div class="centennial-logo <?php echo $logo_class; ?>">
							<a href="<?php echo $exhibitHomeURI ?>">
								<img src="<?php echo img($logo_file); ?>" alt="OUR DUKE: Constructing A Century" />
							</a>
						</div>

					<div class="title-bar" data-responsive-toggle="centennial-menu" data-hide-for="large">
						<button type="button" data-toggle="centennial-menu">Menu</button>
					</div>
					
					<nav class="top-bar grid-container" id="centennial-menu" data-topbar="" role="navigation" aria-label="Main menu">
					    
						<ul class="links vertical large-horizontal menu" data-responsive-menu="drilldown large-dropdown">
								
							<li><a href="/exhibits/show/ourduke/introduction">Intro<span class="hide-for-large">duction</span></a></li>

							<li>
								<a href="/exhibits/show/ourduke/curators">Curators<span class="hide-for-large">' Statements</span></a>
								<ul class="menu">
                  <li><a href="/exhibits/show/ourduke/curators/caroline">Caroline</a></li>
                  <li><a href="/exhibits/show/ourduke/curators/prisha">Prisha</a></li>
									<li><a href="/exhibits/show/ourduke/curators/zoe">Zoe</a></li>
									<li><a href="/exhibits/show/ourduke/curators/melody">Melody</a></li>
								</ul>
							</li>

							<li>
								<a href="/exhibits/show/ourduke/explore">Explore</a>
								<ul class="menu">
									<li><a href="/exhibits/show/ourduke/explore/section-1">1920s</a></li>
                  <li><a href="/exhibits/show/ourduke/explore/section-2">1930s</a></li>
                  <li><a href="/exhibits/show/ourduke/explore/section-3">1940s</a></li>
                  <li><a href="/exhibits/show/ourduke/explore/section-4">1950s</a></li>
                  <li><a href="/exhibits/show/ourduke/explore/section-5">1960s</a></li>
                  <li><a href="/exhibits/show/ourduke/explore/section-6">1970s</a></li>
									<li><a href="/exhibits/show/ourduke/explore/section-7">1980s</a></li>
                  <li><a href="/exhibits/show/ourduke/explore/section-8">1990s</a></li>
                  <li><a href="/exhibits/show/ourduke/explore/section-9">2000s</a></li>
                  <li><a href="/exhibits/show/ourduke/explore/section-10">2010s</a></li>
                  <li><a href="/exhibits/show/ourduke/explore/section-11">2020s</a></li>
								</ul>
							</li>

							<li><a href="/exhibits/show/ourduke/events">Events</a></li>

							<li><a href="/exhibits/show/ourduke/more-duke-history">More <span class="hide-for-large">Duke </span>History</a></li>

							<li><a href="/exhibits/show/ourduke/your-duke">Your Duke</a></li>

						</ul>

					</nav>

				</section>
			</div>
		</div>
	</div>

</header>
