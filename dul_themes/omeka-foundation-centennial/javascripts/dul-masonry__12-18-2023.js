
jQuery(document).ready(function () {
    
    Omeka.skipNav();
    
    jQuery(document).foundation();

    const fullUrl = window.location.href;
    const segments = fullUrl.split('/');
    const lastSegment = segments.pop();
    const containsSection = lastSegment.includes('section-');
    const containsExplore = lastSegment.includes('explore');
    const searchParams = new URLSearchParams(window.location.search);

    // if show page
    if (jQuery('body').hasClass('exhibits show')) {

        var $allItems = jQuery('.masonry-item').get(); // Keep reference to all items
    
        var $masonryGrid = jQuery('#items').masonry({
            itemSelector: '.masonry-item',
            gutter: 0,
            stagger: 30,
            horizontalOrder: true
        });

        function setLoaderPadding() {
            var loaderBottom = jQuery('#loader').offset().top + jQuery('#loader').outerHeight();
            var dateWrapperTop = jQuery('#date-wrapper').offset().top;
            var loader_bottom_padding = dateWrapperTop - loaderBottom;
            jQuery('#loader').css('top', jQuery('#items').offset().top + 'px');
            jQuery('#loader').css('padding-bottom', loader_bottom_padding + 'px');
            console.log('loader_bottom_padding: ' + loader_bottom_padding);
        }
        
        // setLoaderPadding();
        jQuery('#loader').show();

        function refreshGrid() {
            // setLoaderPadding();
            jQuery('#loader').show();
            $masonryGrid.imagesLoaded().always(function() {
                jQuery('#loader').finish().delay(3000).fadeOut()
                jQuery('#loader').hide();
                $masonryGrid.masonry('layout');
                scrollToItems();
            });
        }

        if (containsSection) {
            refreshGrid();
        }

        if (containsExplore) {

            if (title || tags) {
                jQuery('#loader').show();
                jQuery('#items').show();
                refreshGrid();
            } else {
                jQuery('#loader').hide();
                jQuery('#items').hide();
            }
        }

    }
    
    function scrollToItems() {
        jQuery('html, body').animate({
            // scrollTop: jQuery('#items').offset().top
            scrollTop: jQuery('#filter-anchor').offset().top - 16
        }, 'slow');
    }

    function filterItems() {
        
        jQuery('#items').show();

        var checkedTags = jQuery('.tag-checkbox:checked').map(function() {
            return this.value.trim();
        }).get();

        var searchTitle = jQuery('#title-search').val().toLowerCase().trim();
        $masonryGrid.empty();

        if (jQuery('#date-sort').prop('checked')) {
            var sort = 'date';
            // $masonryGrid.masonry('destroy');
        }

        var sortedItems;

        // console.log('Sort: ' + sort);
        // console.log('++++++');

        if (sort === 'date') {
            sortedItems = $allItems.slice().sort(function(a, b) {
                var dateA = jQuery(a).data('date');
                var dateB = jQuery(b).data('date');

                var yearA = dateA ? parseInt(dateA, 10) : 3000;
                var yearB = dateB ? parseInt(dateB, 10) : 3000;

                return yearA - yearB;
             });
        } else {
            // Shuffle items randomly
            sortedItems = $allItems.slice().sort(() => 0.5 - Math.random());
        }

        sortedItems.forEach(function(item) {
            var $item = jQuery(item);
            
            var itemTags = $item.data('tags').split(' ');
            
            var itemTitle = $item.find('.item-title').text().toLowerCase();
            var itemKeywords = $item.data('keywords') ? $item.data('keywords').split(';') : [];
            var itemText = [itemTitle].concat(itemKeywords.map(keyword => keyword.trim().toLowerCase())).join(' ');

            // console.log(itemText);
            // console.log($item.data('date'));

            var caseKey = `${checkedTags.length > 0 ? 'T' : 'F'}-${searchTitle ? 'T' : 'F'}`;
            var showItem = false;

            switch (caseKey) {
                case 'T-T': // 'true-true; both tags and title/keywords
                    showItem = itemTags.some(tag => checkedTags.includes(tag)) || itemText.includes(searchTitle);
                    break;
                case 'T-F': // true-false; tags only
                    showItem = itemTags.some(tag => checkedTags.includes(tag));
                    break;
                case 'F-T': // false-true; title/keywords only
                    showItem = itemText.includes(searchTitle);
                    break;
                case 'F-F': // false-false; no tags or title/keywords
                    showItem = true;
                    break;
            }

            if (showItem) {
                $masonryGrid.append($item);
            }
        });

        $masonryGrid.masonry('reloadItems');
        refreshGrid();
        updateURLParams();
    }


    function updateURLParams() {
        var checkedTags = jQuery('.tag-checkbox:checked').map(function () {
            return encodeURIComponent(this.value.trim());
        }).get();

        var searchTitle = encodeURIComponent(jQuery('#title-search').val().trim());
        var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?tags=' + checkedTags.join(',') + '&title=' + searchTitle;
        
        // date
        var sort = jQuery('#date-sort').prop('checked') ? 'date' : '';
        if (sort) {
            newurl += '&sort=' + encodeURIComponent(sort);
        }

        window.history.pushState({path: newurl}, '', newurl);
    }


    function setFiltersFromURL() {
        var searchParams = new URLSearchParams(window.location.search);

        var tags = searchParams.get('tags');
        if (tags) {
            tags.split(',').forEach(function (tag) {
                jQuery('.tag-checkbox[value="' + decodeURIComponent(tag) + '"]').prop('checked', true);
            });
        }

        var title = searchParams.get('title');
        if (title) {
            jQuery('#title-search').val(decodeURIComponent(title));
        }

        var sort = searchParams.get('sort');
        if (sort && sort === 'date') {
            jQuery('#date-sort').prop('checked', true);
        }
    }


    jQuery('#date-sort').on('change', function() {
        updateURLParams();
    });

    // if show page
    if (jQuery('body').hasClass('exhibits show')) {
        
        setFiltersFromURL();
        
        jQuery('.tag-checkbox, #title-search, #date-sort').on('change keyup', filterItems);

        // filter buttons
        var tags = searchParams.get('tags');
        var title = searchParams.get('title');

        if (title || tags) {
            
            filterItems();
            
            // scrollToItems();
            
            jQuery('#showFilters').hide();
        
        } else {
            jQuery('#filter-wrapper').hide();
        }

        jQuery('#showFilters').click(function() {
            jQuery('#filter-wrapper').slideToggle();
            jQuery('#showFilters').hide();
        });
        jQuery('#hideFilters').click(function() {
            jQuery('#filter-wrapper').slideToggle();
            jQuery('#showFilters').show();
        });
    }
    
});