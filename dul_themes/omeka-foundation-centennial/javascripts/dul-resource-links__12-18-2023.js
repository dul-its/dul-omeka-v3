jQuery(document).ready(function () {

  // if show page
  if (jQuery('body').hasClass('exhibits show')) {

    // resources page
    jQuery('#resource-1').click(function() {
      window.open('https://exhibits.library.duke.edu/exhibits/show/parapsychology/about-the-exhibit', '_blank');
    });
    jQuery('#resource-2').click(function() {
        window.open('https://exhibits.library.duke.edu/exhibits/show/-cherry-blossoms-among-magnoli/introduction-from-exhibit-crea', '_blank');
    });
    jQuery('#resource-3').click(function() {
        window.open('https://exhibits.library.duke.edu/exhibits/show/desegregation/intro', '_blank');
    });
    jQuery('#resource-4').click(function() {
        window.open('https://exhibits.library.duke.edu/exhibits/show/black-students-matter--allen-/introduction', '_blank');
    });
    jQuery('#resource-5').click(function() {
        window.open('https://exhibits.library.duke.edu/exhibits/show/dukeintlstudents/authors-note', '_blank');
    });
    jQuery('#resource-6').click(function() {
        window.open('https://exhibits.library.duke.edu/exhibits/show/latinx/intro', '_blank');
    });
    jQuery('#resource-7').click(function() {
        window.open('https://exhibits.library.duke.edu/exhibits/show/blomquist2018/intro', '_blank');
    });
    jQuery('#resource-8').click(function() {
        window.open('https://exhibits.library.duke.edu/exhibits/show/timereopened/introduction', '_blank');
    });
    jQuery('#resource-9').click(function() {
        window.open('https://exhibits.library.duke.edu/exhibits/show/chapel2016/intro', '_blank');
    });
    jQuery('#resource-10').click(function() {
        window.open('https://exhibits.library.duke.edu/exhibits/show/queer/intro', '_blank');
    });
    jQuery('#resource-11').click(function() {
        window.open('https://exhibits.library.duke.edu/exhibits/show/fromcampustocockpit/intro', '_blank');
    });
    jQuery('#resource-12').click(function() {
        window.open('https://exhibits.library.duke.edu/exhibits/show/duke175/welcome', '_blank');
    });
    jQuery('#resource-13').click(function() {
        window.open('https://library.duke.edu/rubenstein/collections/creators/corporations/silent-vigil', '_blank');
    });
    jQuery('#resource-14').click(function() {
        window.open('https://library.duke.edu/rubenstein/collections/creators/corporations/duke-presidents', '_blank');
    });
    jQuery('#resource-15').click(function() {
        window.open('https://guides.library.duke.edu/c.php?g=1316548', '_blank');
    });
    jQuery('#resource-16').click(function() {
        window.open('https://library.duke.edu/rubenstein/uarchives', '_blank');
    });
    
    
  };

});