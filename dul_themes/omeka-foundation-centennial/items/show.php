<?php 
$layout = (get_theme_option('item_show_columns') !== null) ? get_theme_option('item_show_columns') : 'single';
$mediaPosition = (get_theme_option('media_position') !== null) ? get_theme_option('media_position') : 'top';
$mediaDisplay = get_theme_option('item_show_media_display');
$showLayout = get_theme_option('item_show_inline_metadata');

if ($mediaDisplay == 'lightgallery') {
    queue_lightgallery_assets();
}
echo head(array('title' => metadata('item', array('Dublin Core', 'Title')),'bodyclass' => 'resource items show ' . $layout)); 

?>


<div class="wrap">

    <?php

    $myItemElements = item_type_elements($item);

    ?>



    <div class="breadcrumb">
    <p><a href="/exhibits/show/ourduke/explore">Explore</a> &raquo;</p>
    </div>

    <div class="content-header">

    <?php 
    
        $titleText = metadata('item', array('Dublin Core', 'Title'));

        echo '<h1 class="item-title">' . $titleText . '</h1>';

    ?>

    </div>


    <?php //echo all_element_texts('item'); ?>

    <?php

    $itemID = metadata('item', 'id');

    ?>

    <div class="grid-x grid-margin-x">

        <!-- IMAGE / TAGS -->

        <div class="cell large-6 large-push-6 margin-bottom-3">
            <div class="grid-x grid-margin-x item-image-wrapper align-center">

                <?php

                // The following returns all of the files associated with an item.
                if (metadata('item', 'has files')): ?>

                <div class="cell large-12">

                    <div id="itemfiles" class="element">
                        <div class="element-text"><?php echo files_for_item(
                            array('imageSize' => 'fullsize')
                        ); ?></div>
                    </div>

                </div>

            <?php endif; ?>

            </div>
        </div>

        <!-- METADATA -->

        <div class="cell large-6 large-pull-6">

        <?php

            $uniqueTags = array();
            $uniqueDecadeTags = array();
            $decadeTags = array('1920s', '1930s', '1940s', '1950s', '1960s', '1970s', '1980s', '1990s', '2000s', '2010s', '2020s');
            $sectionTags = array('section-1', 'section-2', 'section-3', 'section-4', 'section-5', 'section-6', 'section-7', 'section-8', 'section-9', 'section-10', 'section-11');

            $tags = $item->getTags();
            foreach ($tags as $tag) {
                $tagName = metadata($tag, 'name');
                if (!in_array($tagName, $uniqueTags) && !in_array($tagName, $decadeTags)) {
                    $uniqueTags[] = $tagName;
                }
            }
            sort($uniqueTags);
            foreach ($tags as $tag) {
                $tagName = metadata($tag, 'name');
                if (!in_array($tagName, $uniqueDecadeTags) && in_array($tagName, $decadeTags)) {
                    $uniqueDecadeTags[] = $tagName;
                }
            }


            echo '<div id="single-item-tags" class="tags-left">';

            foreach ($uniqueTags as $tag) {
                echo '<a class="tag" href="/exhibits/show/ourduke/explore?tags=' . str_replace(' ', '', html_escape($tag)) . '">';
                echo '<span>' . html_escape($tag) . '</span>';
                echo '</a>';
            }

            foreach ($uniqueDecadeTags as $tag) {
                $position = array_search($tag, $decadeTags);
                echo '<a class="decade" href="/exhibits/show/ourduke/explore/' . $sectionTags[$position] . '">';
                echo '<span>' . html_escape($tag) . '</span>';
                echo '</a>';
            }
            echo '</div>';

        ?>

        <dl class="left-metadata">

            <!-- Title -->
            <?php if (metadata($item, array('Dublin Core', 'Title')) != "") {
                echo '<dt>Title:</dt>';
                echo '<dd>' . metadata($item, array('Dublin Core', 'Title')) . '</dd>';
            };
            ?>

            <!-- Description -->
            <?php if ( metadata($item, array('Dublin Core', 'Description')) != "" || $myItemElements['Item Link'] ) {
                echo '<dt>Description:</dt>';
                echo '<dd>';
                if (metadata($item, array('Dublin Core', 'Description')) != "") {
                    echo metadata($item, array('Dublin Core', 'Description'));
                }
                if ($myItemElements['Item Link']) {
                    echo '<div class="item-more"><a class="button radius" href="' . $myItemElements['Item Link'] . '" target="_blank">Learn More</a></div>';
                }
                echo '</dd>';
            };
            ?>

            <!-- Display Date -->
            <?php
            if ($myItemElements['Display Date']) {
                echo '<dt>Date:</dt>';
                echo '<dd>' . $myItemElements['Display Date'] . '</dd>';
            };
            ?>  
        
            <!-- Creator -->
            <?php if (metadata($item, array('Dublin Core', 'Creator')) != "") {

            $myItemCreator = metadata($item, array('Dublin Core', 'Creator'));

                echo '<dt>Creator(s):</dt>';
                echo '<dd>' . $myItemCreator . '</dd>';

            };
            ?>


            <!-- Contributor -->
            <?php if (metadata($item, array('Dublin Core', 'Contributor')) != "") {
                echo '<dt>Contributor(s):</dt>';
                echo '<dd>' . metadata($item, array('Dublin Core', 'Contributor')) . '</dd>';
            };
            ?>

            <!-- Source -->
            <?php if (metadata($item, array('Dublin Core', 'Source')) != "") {
                echo '<dt>Source:</dt>';
                echo '<dd>' . metadata($item, array('Dublin Core', 'Source')) . '</dd>';
            };
            ?>

            <!-- Rights -->
            <?php if (metadata($item, array('Dublin Core', 'Rights')) != "") {
                echo '<dt>Rights:</dt>';
                if (metadata($item, array('Dublin Core', 'Rights')) == 'CC BY-NC-SA 4.0 DEED') {
                    echo '<dd><a href="https://creativecommons.org/licenses/by-nc-sa/2.0/" target="_blank" title="Creative Commons -- Attribution-NonCommercial-ShareAlike 2.0 Generic"><img src="/themes/omeka-foundation-centennial/images/creative-commons.png" alt="CC BY-NC-SA 4.0 DEED" class="creative-commons" /> Some Rights Reserved</a></dd>';
                } else {
                echo '<dd>' . metadata($item, array('Dublin Core', 'Rights')) . '</dd>';
                }
            };
            ?>

            <!-- Citation -->
            <?php
            if ($myItemElements['Citation']) {
                echo '<dt>Citation:</dt>';
                echo '<dd>' . $myItemElements['Citation'] . '</dd>';
            };
            ?>

            <?php fire_plugin_hook('public_items_show', array('view' => $this, 'item' => $item)); ?>

        </dl>

        </div>


        

    </div>



    <div class="grid-x grid-margin-x" id="exhibit-page-navigation">

    <div class="cell large-6 text-left">
        <?php if ($prevID != NULL) { ?>
        <a href="/exhibits/show/lincoln/item/<?php echo $prevID; ?>" class="previous-item">← View Previous Item</a>
        <?php } ?>
    </div>

    <div class="cell large-6 text-right">
        <?php if ($nextID != NULL) { ?>
        <a href="/exhibits/show/lincoln/item/<?php echo $nextID; ?>" class="next-item">View Next Item →</a>
        <?php } ?>
    </div>

    </div>

    <div class="grid-x grid-margin-x item-navigation">

    <div class="large-12 cell text-center">
        <a href="/exhibits/show/lincoln/explore/<?php echo $myItemSection ?>"><?php echo $myDisplayCat; ?></a>
    </div>

    <p>&nbsp;</p>

    </div>

</div>
    


<?php echo foot(); ?>
