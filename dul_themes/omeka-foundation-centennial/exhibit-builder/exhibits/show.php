<?php
// this gets url slug
// echo basename(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));


$_SERVER['REQUEST_URI_PATH'] = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$URL_SEGMENTS = explode('/', rtrim($_SERVER['REQUEST_URI_PATH'], '/'));
$URL_SEGMENTS = array_reverse($URL_SEGMENTS);


echo head(array(
    'title' => metadata('exhibit_page', 'title') . ' &middot; ' . metadata('exhibit', 'title'),
    'bodyclass' => 'exhibits show'));
?>

<div class="grid-x grid-margin-x">

    <div id="exhibit-content" class="cell large-12">

        <div id="exhibit-blocks">

        <?php exhibit_builder_render_exhibit_page(); ?>

        <?php

        # $collectionID = 14;
        $collectionID = 347;

        // Explore (all items)
        if ($URL_SEGMENTS[0] == 'explore' && $URL_SEGMENTS[1] == 'ourduke') {
          $items = get_records('Item', array(
            'collection' => $collectionID
          ), 999);
        }

        // Section 1: 1920s
        if ($URL_SEGMENTS[0] == 'section-1' && $URL_SEGMENTS[2] == 'ourduke') {
          $items = get_records('Item', array(
            'collection' => $collectionID,
            'tags' => '1920s'
          ), 999);
        }
        // Section 2: 1930s
        if ($URL_SEGMENTS[0] == 'section-2' && $URL_SEGMENTS[2] == 'ourduke') {
          $items = get_records('Item', array(
            'collection' => $collectionID,
            'tags' => '1930s'
          ), 999);
        }
        // Section 3: 1940s
        if ($URL_SEGMENTS[0] == 'section-3' && $URL_SEGMENTS[2] == 'ourduke') {
          $items = get_records('Item', array(
            'collection' => $collectionID,
            'tags' => '1940s'
          ), 999);
        }
        // Section 4: 1950s
        if ($URL_SEGMENTS[0] == 'section-4' && $URL_SEGMENTS[2] == 'ourduke') {
          $items = get_records('Item', array(
            'collection' => $collectionID,
            'tags' => '1950s'
          ), 999);
        }
        // Section 5: 1960s
        if ($URL_SEGMENTS[0] == 'section-5' && $URL_SEGMENTS[2] == 'ourduke') {
          $items = get_records('Item', array(
            'collection' => $collectionID,
            'tags' => '1960s'
          ), 999);
        }
        // Section 6: 1970s
        if ($URL_SEGMENTS[0] == 'section-6' && $URL_SEGMENTS[2] == 'ourduke') {
          $items = get_records('Item', array(
            'collection' => $collectionID,
            'tags' => '1970s'
          ), 999);
        }
        // Section 7: 1980s
        if ($URL_SEGMENTS[0] == 'section-7' && $URL_SEGMENTS[2] == 'ourduke') {
          $items = get_records('Item', array(
            'collection' => $collectionID,
            'tags' => '1980s'
          ), 999);
        }
        // Section 8: 1990s
        if ($URL_SEGMENTS[0] == 'section-8' && $URL_SEGMENTS[2] == 'ourduke') {
          $items = get_records('Item', array(
            'collection' => $collectionID,
            'tags' => '1990s'
          ), 999);
        }
        // Section 9: 2000s
        if ($URL_SEGMENTS[0] == 'section-9' && $URL_SEGMENTS[2] == 'ourduke') {
          $items = get_records('Item', array(
            'collection' => $collectionID,
            'tags' => '2000s'
          ), 999);
        }
        // Section 10: 2010s
        if ($URL_SEGMENTS[0] == 'section-10' && $URL_SEGMENTS[2] == 'ourduke') {
          $items = get_records('Item', array(
            'collection' => $collectionID,
            'tags' => '2010s'
          ), 999);
        }
        // Section 11: 2020s
        if ($URL_SEGMENTS[0] == 'section-11' && $URL_SEGMENTS[2] == 'ourduke') {
          $items = get_records('Item', array(
            'collection' => $collectionID,
            'tags' => '2020s'
          ), 999);
        }

        $sections_array = array(
          'section-1',
          'section-2',
          'section-3',
          'section-4',
          'section-5',
          'section-6',
          'section-7',
          'section-8',
          'section-9',
          'section-10',
          'section-11'
        );

        if ( (in_array($URL_SEGMENTS[0],$sections_array) && $URL_SEGMENTS[2] == 'ourduke') || ($URL_SEGMENTS[0] == 'explore' && $URL_SEGMENTS[1] == 'ourduke' ) ) {
        
          include('render_filters.php');

          ?>

          <div id="loader" style="display: none;">
            <img src="/themes/omeka-foundation-centennial/images/animated_devil_compressed.gif" alt="Loading..." />
          </div>

          <div id="items" class="small-block-grid-1 medium-block-grid-2 large-block-grid-3">

            <?php include('render_items.php'); ?>

        </div>

        <?php
        }
        ?>

        </div>

        <div class="grid-x grid-margin-x" id="exhibit-page-navigation">
            
          <div class="cell large-6 text-left">
            <?php if ($prevLink = exhibit_builder_link_to_previous_page()): ?>
              <?php echo $prevLink; ?>
            <?php endif; ?>
          </div>
          
          <div class="cell large-6 text-right">
            <?php if ($nextLink = exhibit_builder_link_to_next_page()): ?>
              <?php echo $nextLink; ?>
            <?php endif; ?>
          </div>

        </div>
            
        <div class="grid-x grid-margin-x item-navigation">
          <div class="large-12 cell text-center">
            <?php echo exhibit_builder_page_trail(); ?>
          </div>
        </div>

    </div>


</div>


<?php echo foot(); ?>
