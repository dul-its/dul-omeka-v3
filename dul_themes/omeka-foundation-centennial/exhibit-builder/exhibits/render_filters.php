<div id="filter-anchor"></div>
<div id="filter-wrapper">

  <div id="text-filter">
    <input type="text" id="title-search" placeholder="Search" />
    <input type="checkbox" id="date-sort" /> <label for="date-sort"><span>Sort by Date</span></label>
  </div>

<?php 

$uniqueTags = array();
$decadeTags = array('1920s', '1930s', '1940s', '1950s', '1960s', '1970s', '1980s', '1990s', '2000s', '2010s', '2020s');

foreach ($items as $item) {
  $tags = $item->getTags();
  foreach ($tags as $tag) {
    $tagName = metadata($tag, 'name');
    if (!in_array($tagName, $uniqueTags) && !in_array($tagName, $decadeTags)) {
        $uniqueTags[] = $tagName;
    }
  }
}

sort($uniqueTags);

$count = 1;

echo '<div id="item-tags">';

foreach ($uniqueTags as $tag) {
  echo '<input type="checkbox" id="tag-' . $count .'" class="tag-checkbox" value="' . str_replace(' ', '', html_escape($tag)) . '" />';
  echo '<label for="tag-' . $count . '" class="tag-label">';
  echo '<span>' . html_escape($tag) . '</span>';
  echo '</label>';
  $count++;
}
echo '</div>';

// echo '<div id="item-decades">';
// foreach ($decadeTags as $tag) {
//   echo '<input type="checkbox" id="tag-' . $count .'" class="tag-checkbox" value="' . str_replace(' ', '', html_escape($tag)) . '" />';
//   echo '<label for="tag-' . $count . '" class="tag-label">';
//   echo '<span>' . html_escape($tag) . '</span>';
//   echo '</label>';
//   $count++;
// }
// echo '</div>';

?>

  <div class="filter-toggle">
    <button id="hideFilters"><span class="show-for-sr"></span>Hide Filters</a></button>
  </div>

</div>

  <div class="filter-toggle">
    <button id="showFilters"><span class="show-for-sr"></span>Show Filters</button>
  </div>
