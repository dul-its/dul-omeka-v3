<?php

foreach ($items as $item) {
    set_current_record('item', $item);
    $tags = $item->getTags();
    $tagsArray = array();
    foreach ($tags as $tag) {
        $tagName = metadata($tag, 'name');
        array_push( $tagsArray, str_replace(' ', '', html_escape($tag)) );
    }
    $tagString = implode(' ', $tagsArray);
    $myItemElements = item_type_elements($item);

    if (isset($myItemElements['Alternative Text'])) {
        $alt = $myItemElements['Alternative Text'];
    } else {
        $alt = metadata($item, array('Dublin Core', 'Title'));
    }
?>

<div class="masonry-item" data-id="<?php echo metadata($item, 'id');?>" data-tags="<?php echo $tagString;?>" data-date="<?php echo metadata($item, array('Dublin Core', 'Date')); ?>" data-keywords="<?php echo $myItemElements['Keywords']; ?>">
  <a href="<?php echo exhibit_builder_exhibit_item_uri($item); ?>">
      <div class="text-wrapper item-title">
        <?php echo metadata($item, array('Dublin Core', 'Title')); ?>
      </div>
      <!-- <div class="date"> -->
        <?php # echo metadata($item, array('Dublin Core', 'Date')); ?>
      <!-- </div> -->
      <div class="image-wrapper">
        <?php 
          if (metadata($item, 'id') == 13517) {
            echo '<img alt="" src="/themes/omeka-foundation-centennial/images/media/centennial_video_thumb.jpg" title="Excerpt from a 1954 promotional video">';
          } elseif (metadata($item, 'id') == 13407) {
            echo '<img alt="" src="/themes/omeka-foundation-centennial/images/media/centennial_mp3_thumb.jpg" title="Catherine M. Wilfert Oral History Interview">';
          } else {
            echo item_image('fullsize', array('alt' => $alt)); 
          }
        ?>
      </div>
    </div>
   </a>
<?php
  }
?>

