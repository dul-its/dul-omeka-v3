<?php
// this gets url slug
// echo basename(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));


$_SERVER['REQUEST_URI_PATH'] = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$URL_SEGMENTS = explode('/', rtrim($_SERVER['REQUEST_URI_PATH'], '/'));
$URL_SEGMENTS = array_reverse($URL_SEGMENTS);


echo head(array(
    'title' => metadata('exhibit_page', 'title') . ' &middot; ' . metadata('exhibit', 'title'),
    'bodyclass' => 'exhibits show'));
?>

<div class="grid-x grid-margin-x">

    <div id="exhibit-content" class="cell large-12">

        <div id="exhibit-blocks">

        <?php exhibit_builder_render_exhibit_page(); ?>

        <?php

        // Section 1: 1809–1853 Early Years and Ambitions
        if ($URL_SEGMENTS[0] == 'section-1' && $URL_SEGMENTS[2] == 'lincoln') {
          $items = get_records('Item',array('tags'=>'lincoln-section-1'),0);

          ?>
          <ul id="masonry-container" class="small-block-grid-1 medium-block-grid-2 large-block-grid-3">

            <?php include('render_items.php'); ?>

          </ul>

        <?php
        }
        ?>


        <?php

        // Section 2: 1854–1860 A National Stage
        if ($URL_SEGMENTS[0] == 'section-2' && $URL_SEGMENTS[2] == 'lincoln') {
          $items = get_records('Item',array('tags'=>'lincoln-section-2'),0);

          ?>
          <ul id="masonry-container" class="small-block-grid-1 medium-block-grid-2 large-block-grid-3">

            <?php include('render_items.php'); ?>

          </ul>

        <?php
        }
        ?>

        <?php

        // Section 3: 1861–1865 Commander in Chief
        if ($URL_SEGMENTS[0] == 'section-3' && $URL_SEGMENTS[2] == 'lincoln') {
          $items = get_records('Item',array('tags'=>'lincoln-section-3'),0);

          ?>
          <ul id="masonry-container" class="small-block-grid-1 medium-block-grid-2 large-block-grid-3">

            <?php include('render_items.php'); ?>

          </ul>

        <?php
        }
        ?>


        <?php

        // Section 4: Eulogies
        if ($URL_SEGMENTS[0] == 'section-4' && $URL_SEGMENTS[2] == 'lincoln') {
          $items = get_records('Item',array('tags'=>'lincoln-section-4'),0);

          ?>
          <ul id="masonry-container" class="small-block-grid-1 medium-block-grid-2 large-block-grid-3">

            <?php include('render_items.php'); ?>

          </ul>

        <?php
        }
        ?>


        <?php

        // Section 5: Lincoln's Library
        if ($URL_SEGMENTS[0] == 'section-5' && $URL_SEGMENTS[2] == 'lincoln') {
          $items = get_records('Item',array('tags'=>'lincoln-section-5'),0);
          ?>

          <ul id="masonry-container" class="small-block-grid-1 medium-block-grid-2 large-block-grid-3">

            <?php include('render_items.php'); ?>

          </ul>

        <?php
        }
        ?>

        </div>

        <div class="grid-x grid-margin-x" id="exhibit-page-navigation">
            
          <div class="cell large-6 text-left">
            <?php if ($prevLink = exhibit_builder_link_to_previous_page()): ?>
              <?php echo $prevLink; ?>
            <?php endif; ?>
          </div>
          
          <div class="cell large-6 text-right">
            <?php if ($nextLink = exhibit_builder_link_to_next_page()): ?>
              <?php echo $nextLink; ?>
            <?php endif; ?>
          </div>

        </div>
            
        <div class="grid-x grid-margin-x item-navigation">
          <div class="large-12 cell text-center">
            <?php echo exhibit_builder_page_trail(); ?>
          </div>
        </div>

    </div>


</div>


<?php echo foot(); ?>
