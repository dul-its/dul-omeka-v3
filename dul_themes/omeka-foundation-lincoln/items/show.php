<?php 
$layout = (get_theme_option('item_show_columns') !== null) ? get_theme_option('item_show_columns') : 'single';
$mediaPosition = (get_theme_option('media_position') !== null) ? get_theme_option('media_position') : 'top';
$mediaDisplay = get_theme_option('item_show_media_display');
$showLayout = get_theme_option('item_show_inline_metadata');

if ($mediaDisplay == 'lightgallery') {
    queue_lightgallery_assets();
}
echo head(array('title' => metadata('item', array('Dublin Core', 'Title')),'bodyclass' => 'resource items show ' . $layout)); 

?>


<div class="wrap">

    <?php

    $myItemElements = item_type_elements($item);
    $myItemCreator = "";
    $myItemOrigin = "";
    $myItemCitation = "";

    $myItemIndex = $myItemElements['item-index'];
    $myItemSort = $myItemElements['Item Sort'];
    $myItemSection = $myItemElements['item-section-slug'];
    $myItemTag = 'lincoln-' . $myItemSection;
    $myItemSectionDisplay = $myItemElements['Item Sort'];
    
    $myDisplayCat = str_replace("to", "-", $myItemSort);
    $myDisplayCat = str_replace("-", "&ndash;", $myDisplayCat);

    $allItems = get_records('Item',array('tags' => $myItemTag),0);

    $allItemsIDs = array_map(function($item) {
        return $item->id;
    }, $allItems);

    $myItemPosition = array_search($item->id, $allItemsIDs);

    $prevID = $myItemPosition > 0 ? $allItemsIDs[$myItemPosition - 1] : NULL;
    $nextID = $myItemPosition < count($allItemsIDs) - 1 ? $allItemsIDs[$myItemPosition + 1] : NULL;

    ?>



    <div class="breadcrumb">
    <p><a href="/exhibits/show/lincoln/explore">Explore</a> &raquo; <a href="/exhibits/show/lincoln/explore/<?php echo $myItemSection ?>"><?php echo $myDisplayCat; ?></a> &raquo;</p>
    </div>

    <div class="content-header">

    <?php 
    
        $titleText = metadata('item', array('Dublin Core', 'Title'));

        echo '<h1 class="item-title">' . $titleText . '</h1>';

    ?>

    </div>


    <?php //echo all_element_texts('item'); ?>

    <div class="grid-x grid-margin-x item-image-wrapper">

    <!-- The following returns all of the files associated with an item. -->
    <?php if (metadata('item', 'has files')): ?>
    
        <div class="cell large-12">

            <div id="itemfiles" class="element">
                <div class="element-text"><?php echo files_for_item(
                    array('imageSize' => 'fullsize')
                ); ?></div>
            </div>

        </div>

    <?php endif; ?>

    </div>

    <div class="grid-x grid-margin-x item-content-wrapper">

    <div class="cell large-1 left">&nbsp;</div>
    <div class="cell large-10">

        <dl>

            <!-- Creator -->
            <?php if (metadata($item, array('Dublin Core', 'Creator')) != "") {

            $myItemCreator = metadata($item, array('Dublin Core', 'Creator'));

                echo '<dt>Creator(s):</dt>';
                echo '<dd>' . $myItemCreator . '</dd>';

            };
            ?>


            <!-- Contributor -->
            <?php if (metadata($item, array('Dublin Core', 'Contributor')) != "") {
                echo '<dt>Contributor(s):</dt>';
                echo '<dd>' . metadata($item, array('Dublin Core', 'Contributor')) . '</dd>';
            };
            ?>


            <!-- Title -->
            <?php if (metadata($item, array('Dublin Core', 'Title')) != "") {
                echo '<dt>Title:</dt>';
                echo '<dd>' . metadata($item, array('Dublin Core', 'Title')) . '</dd>';
            };
            ?>

            <!-- Description -->
            <?php if (metadata($item, array('Dublin Core', 'Description')) != "") {
                echo '<dt>Description:</dt>';
                echo '<dd>' . metadata($item, array('Dublin Core', 'Description')) . '</dd>';
            };
            ?>

            <!-- Source -->
            <?php if (metadata($item, array('Dublin Core', 'Source')) != "") {
                echo '<dt>Source:</dt>';
                echo '<dd>' . metadata($item, array('Dublin Core', 'Source')) . '</dd>';
            };
            ?>


            <!-- Citation -->

            <dt>Citation:</dt>

            <?php

            if ($myItemElements['Citation']) {
            echo '<dd>' . $myItemElements['Citation'] . '</dd>';
            };

            ?>

            <?php fire_plugin_hook('public_items_show', array('view' => $this, 'item' => $item)); ?>

        </dl>

    </div>
    <div class="cell large-1 right">&nbsp;</div>

    </div>

    <div class="grid-x grid-margin-x" id="exhibit-page-navigation">

    <div class="cell large-6 text-left">
        <?php if ($prevID != NULL) { ?>
        <a href="/exhibits/show/lincoln/item/<?php echo $prevID; ?>" class="previous-item">← View Previous Item</a>
        <?php } ?>
    </div>

    <div class="cell large-6 text-right">
        <?php if ($nextID != NULL) { ?>
        <a href="/exhibits/show/lincoln/item/<?php echo $nextID; ?>" class="next-item">View Next Item →</a>
        <?php } ?>
    </div>

    </div>

    <div class="grid-x grid-margin-x item-navigation">

    <div class="large-12 cell text-center">
        <a href="/exhibits/show/lincoln/explore/<?php echo $myItemSection ?>"><?php echo $myDisplayCat; ?></a>
    </div>

    <p>&nbsp;</p>

    </div>




</div>


<?php echo foot(); ?>
