<header role="banner" class="l-header">

	<div class="masthead-wrapper">
    <div class="grid-x">
      <div class="small-12">
        <div class="grid-container masthead">

          <div class="library-logo">
            <a href="//library.duke.edu" title="Duke University Libraries"><img src="/themes/omeka-foundation-dul/images/dul/dul_logo.png" alt="Duke University Libraries"></a>
          </div>

          <div class="exhibits-logo">
            <a href="//library.duke.edu/exhibits" title="Library Exhibits"><img src="/themes/omeka-foundation-dul/images/dul/exhibits_logo.png" alt="Exhibits"></a>
          </div>
        
        </div>
      </div>
    </div>
	</div>


	<div class="nav-wrapper">
		<div class="grid-x">
			<div class="small-12">
				<section class="top-bar-section text-center" id="nav">

					<?php
						// get exhibit homepage link
						$exhibitSlug = 'lincoln';
						$exhibitHomeURI = html_escape(WEB_ROOT) . '/exhibits/show/' . $exhibitSlug;
						?>

						<div class="lincoln-logo <?php echo $logo_class; ?>">
							<a href="<?php echo $exhibitHomeURI ?>">
								<img src="<?php echo img($logo_file); ?>" alt="To Stand by the Side of Freedom: Abraham Lincoln and 19th Century America -- Selections from the David M. Rubenstein Americana Collection" />
							</a>
						</div>

					<div class="title-bar" data-responsive-toggle="lincoln-menu" data-hide-for="large">
						<button type="button" data-toggle="lincoln-menu">Menu</button>
					</div>
					
					<nav class="top-bar grid-container" id="lincoln-menu" data-topbar="" role="navigation" aria-label="Main menu">
					    <ul class="links vertical medium-horizontal menu dropdown" data-dropdown-menu>
								

							<li><a href="/exhibits/show/lincoln/introduction">Introduction</a></li>

							<li><a href="/exhibits/show/lincoln/about">About</a></li>

							<li>
								<a href="/exhibits/show/lincoln/explore">Explore</a>
								<ul class="menu">
									<li><a href="/exhibits/show/lincoln/explore/section-1">1809&ndash;1853 Early Years and Ambitions</a></li>
                  <li><a href="/exhibits/show/lincoln/explore/section-2">1854&ndash;1860 A National Stage</a></li>
                  <li><a href="/exhibits/show/lincoln/explore/section-3">1861&ndash;1865 Commander in Chief</a></li>
                  <li><a href="/exhibits/show/lincoln/explore/section-4">Eulogies</a></li>
                  <li><a href="/exhibits/show/lincoln/explore/section-5">Lincoln's Library</a></li>
                  <li><a href="/exhibits/show/lincoln/explore/section-6">Emancipation Proclamation and 13th Amendment</a></li>
								</ul>
							</li>

							<li><a href="/exhibits/show/lincoln/events">Events</a></li>

							<li><a href="/exhibits/show/lincoln/lincolns-19th-century">Lincoln's 19<sup>th</sup> Century</a></li>


							</ul>
					</nav>

				</section>
			</div>
		</div>
	</div>

</header>
