</div></div><!-- end content -->
    
    <div id="date-wrapper">
        
        <section class="exhibit-dates">

            <div class="grid-x">

                <div class="large-12 cell text-center">
                    <p>November 16, 2021 &ndash; May 14, 2022<br /><span class="location">Duke University Libraries</span>
                </div>

            </div>

        </section>
    
    </div>

    <div id="footer-wrapper">

        <footer role="contentinfo">

            <div class="grid-x">
                <div class="large-1 cell devillogo">
                    <a href="https://library.duke.edu"><img alt="Duke University Libraries" src="/themes/omeka-foundation-lincoln/images/dul/devillogo-60pct-60.png"></a>
                </div>
                
                <div class="large-2 cell">
                    <div class="right-gutter">
                        <h2><a href="https://library.duke.edu/about/contact">Contact Us</a></h2>
                        <address class="small">
                            411 Chapel Drive<br>
                            Durham, NC 27708<br>
                            (919) 660-5870<br>
                            Perkins Library Service Desk
                        </address>
                    </div>
                </div>

                <div class="large-4 cell">
                    <div class="right-gutter">
                        <h2><a href="/services">Services for...</a></h2>
                        <div class="grid-x">
                            <div class="large-7 cell">
                                <ul class="list-unstyled right-gutter">
                                    <li><a href="https://library.duke.edu/services/faculty">Faculty &amp; Instructors</a></li>
                                    <li><a href="https://library.duke.edu/services/graduate">Graduate Students</a></li>
                                    <li><a href="https://library.duke.edu/services/undergraduate">Undergraduate Students</a></li>
                                    <li><a href="https://library.duke.edu/services/international">International Students</a></li>
                                </ul>
                            </div>

                            <div class="large-5 cell">
                                <ul class="list-unstyled right-gutter">
                                    <li><a href="https://library.duke.edu/services/alumni">Alumni</a></li>
                                    <li><a href="https://library.duke.edu/services/donors">Donors</a></li>
                                    <li><a href="https://library.duke.edu/services/visitors">Visitors</a></li>
                                    <li><a href="https://library.duke.edu/services/disabilities">Patrons with Disabilities</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="large-3 cell">
                    
                    <div class="social-media-icons right-gutter">

                        <a href="https://twitter.com/DukeLibraries" title="Twitter"><i class="fab fa-twitter"></i><span class="sr-only">Twitter</span></a>

                        <a href="https://www.facebook.com/dukelibraries" title="Facebook"><i class="fab fa-facebook"></i><span class="sr-only">Facebook</span></a>

                        <a href="https://www.youtube.com/user/DukeUnivLibraries" title="Youtube"><i class="fab fa-youtube"></i><span class="sr-only">Youtube</span></a>

                        <a href="https://www.flickr.com/photos/dukeunivlibraries/" title="Flickr"><i class="fab fa-flickr"></i><span class="sr-only">Flickr</span></a>

                        <a href="https://instagram.com/dukelibraries" title="Instagram"><i class="fab fa-instagram"></i><span class="sr-only">Instagram</span></a>

                        <a href="https://blogs.library.duke.edu/" title="RSS"><i class="fas fa-rss-square"></i><span class="sr-only">RSS</span></a>

                    </div>

                    <ul class="list-unstyled">
                        <li><a href="https://library.duke.edu/about/newsletter">Sign Up for Our Newsletter</a></li>
                        <li><a href="https://library.duke.edu/about/reuse-attribution">Re-use &amp; Attribution</a> / <a href="/about/privacy">Privacy</a></li>
                        <li><a href="https://library.duke.edu/about/statement-potentially-harmful-language-library-descriptions">Harmful Language Statement</a></li>
                        <li><a href="https://library.duke.edu/support">Support the Libraries</a></li>
                    </ul>
                    
                    <a rel="license" href="https://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US"><img alt="Creative Commons License" style="border-width:0" src="https://licensebuttons.net/l/by-nc-sa/3.0/80x15.png" title="This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License"></a>
                </div>

                <div class="large-2 cell dukelogo">
                    <div id="duke-logo">
                        <a href="https://www.duke.edu"><img src="/themes/omeka-foundation-lincoln/images/dul/dukelogo_vert_60pct_140.png" alt="Duke University"></a>
                    </div>

                    <div id="omeka_login_url">
                        <a href="/admin">Exhibits Sign In</a>
                    </div>

                </div>

            </div>
            
            <?php fire_plugin_hook('public_footer', array('view' => $this)); ?>

        </footer><!-- end footer -->

    </div><!-- end footer-wrapper -->
    
    </div><!-- end grid-container -->
    </div><!-- end off canvas content -->

    <script type="text/javascript">
    jQuery(document).ready(function () {
        Omeka.skipNav();
        jQuery(document).foundation();

        var $masonryGrid = jQuery('#masonry-container').masonry({
            itemSelector: '.masonry-item',
            gutter: 32
        });

        $masonryGrid.imagesLoaded().progress( function() {
            $masonryGrid.masonry('layout');
        });
    });

    var _paq = window._paq = window._paq || [];
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
        var u="//analytics.lib.duke.edu/";
        _paq.push(['setTrackerUrl', u+'matomo.php']);
        _paq.push(['setSiteId', '21']);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
    })();
    </script>

</body>
</html>
