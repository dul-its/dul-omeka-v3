<?php foreach ($elementsForDisplay as $setName => $setElements): ?>
<div class="element-set">
    <?php foreach ($setElements as $elementName => $elementInfo): ?>
    <div id="<?php echo text_to_id(html_escape("$setName $elementName")); ?>" class="element">

        <h3><?php echo html_escape(__($elementName)); ?>:</h3>

        <?php $i = 0; ?>
        <?php foreach ($elementInfo['texts'] as $text):
        $i++;
        if( $i == 1): ?>
            <div class="element-text"><p><?php echo $text; ?></p></div>
        <?php else: ?>
            <div class="element-text"><p><?php echo $text; ?></p></div>
        <?php endif; ?>
        <?php endforeach; ?>
    </div><!-- end element -->
    <?php endforeach; ?>
</div><!-- end element-set -->
<?php endforeach; ?>