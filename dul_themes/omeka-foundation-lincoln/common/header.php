<?php

// old vars

$_SERVER['REQUEST_URI_PATH'] = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$URL_SEGMENTS = explode('/', rtrim($_SERVER['REQUEST_URI_PATH'], '/'));
$URL_SEGMENTS = array_reverse($URL_SEGMENTS);

if ($URL_SEGMENTS[0] == 'introduction') {
	$logo_class = 'homepage';
	$logo_file = 'lincoln_logo_10-28-2021b.png';
} else {
	$logo_class = 'subpage';
	$logo_file = 'lincoln_logo_subpages_10-28-2021b.png';
}

?>

<!DOCTYPE html>
<html lang="<?php echo get_html_lang(); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php if ($author = option('author')): ?>
    <meta name="author" content="<?php echo $author; ?>" />
    <?php endif; ?>
    <?php if ($copyright = option('copyright')): ?>
    <meta name="copyright" content="<?php echo $copyright; ?>" />
    <?php endif; ?>
    <?php if ( $description = option('description')): ?>
    <meta name="description" content="<?php echo $description; ?>" />
    <?php endif; ?>
    <?php
    if (isset($title)) {
        $titleParts[] = strip_formatting($title);
    }
    $titleParts[] = option('site_title');
    ?>
    <title><?php echo implode(' &middot; ', $titleParts); ?></title>

    <?php echo auto_discovery_link_tags(); ?>

    <!-- Social Sharing -->
    <meta property="og:title"	content="To Stand by the Side of Freedom: Abraham Lincoln and 19th Century America -- Selections from the David M. Rubenstein Americana Collection" />
    <meta property="og:description" content="Selections from David M. Rubenstein's private collection will explore the founding, growth, and problems of American history through the life and presidency of Abraham Lincoln. On exhibit October 7, 2021 – February 23, 2022 at Duke University Libraries" />
    <meta property="og:image" content="https://exhibits.library.duke.edu/themes/omeka-foundation-lincoln/images/lincoln_fb_card_10-28-2021b.png" />
    <meta property="og:image:width"	content="1200" />
    <meta property="og:image:height" content="670" />

    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@DukeLibraries">
    <meta name="twitter:creator" content="@DukeLibraries">
    <meta name="twitter:title" content="To Stand by the Side of Freedom: Abraham Lincoln and 19th Century America -- Selections from the David M. Rubenstein Americana Collection">
    <meta name="twitter:description" content="Selections from David M. Rubenstein's private collection will explore the founding, growth, and problems of American history through the life and presidency of Abraham Lincoln. On exhibit October 7, 2021 – February 23, 2022 at Duke University Libraries">
    <meta name="twitter:image" content="https://exhibits.library.duke.edu/themes/omeka-foundation-lincoln/images/lincoln_fb_card_10-28-2021b.png">


    <!-- Plugin Stuff -->

    <?php fire_plugin_hook('public_head', array('view'=>$this)); ?>


    <!-- Stylesheets -->
    <?php
    $stylesheetOption = (get_theme_option('stylesheet')) ? get_theme_option('stylesheet') : 'default';
    $banner = get_theme_option('banner');
    $bannerWidth = (get_theme_option('banner_width')) ? get_theme_option('banner_width') : '';
    $bannerHeight = get_theme_option('banner_height_desktop');
    if ($bannerHeight == '') {
        $bannerHeight = 'auto';
    }
    $bannerHeightMobile = get_theme_option('banner_height_mobile');
    $bannerPosition = (get_theme_option('banner_position')) ? str_replace('_','-', get_theme_option('banner_position')) : 'center';
    queue_css_file(array('iconfonts'));
    queue_css_file($stylesheetOption);
    queue_css_string('
        .banner {
            height: ' .  $bannerHeight . ';
            background-position: ' . $bannerPosition . ';
        }'
    );
    if ($bannerHeightMobile !== '') {
        queue_css_string('
            @media screen and (max-width:640px) {
                .banner {
                    height: ' . $bannerHeightMobile . ';
                }
            }'
        );
    }
    queue_css_url('//fonts.googleapis.com/css?family=EB+Garamond:400,400i,700|Open+Sans:400,400i,700');
    queue_css_url('//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css');
    queue_css_file('lightbox');
    queue_css_file('dul_lincoln');
    echo head_css();

    echo theme_header_background();
    ?>

    <!-- JavaScripts -->
    <?php
    queue_js_file(array('globals', 'app'));
    queue_js_url('//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/js/fontawesome.min.js');
    queue_js_url('//cdn.jsdelivr.net/npm/foundation-sites@6.7.5/dist/js/foundation.min.js', array(
      'integrity' => 'sha256-/PFxCnsMh+nTuM0k3VJCRch1gwnCfKjaP8rJNq5SoBg= sha384-9ksAFjQjZnpqt6VtpjMjlp2S0qrGbcwF/rvrLUg2vciMhwc1UJJeAAOLuJ96w+Nj sha512-UMSn6RHqqJeJcIfV1eS2tPKCjzaHkU/KqgAnQ7Nzn0mLicFxaVhm9vq7zG5+0LALt15j1ljlg8Fp9PT1VGNmDw==',
      'crossorigin' => 'anonymous',
    ));
    queue_js_file('lightbox.min','javascripts/vendor/lightbox/js');
    queue_js_file('masonry.min','javascripts/vendor/masonry');
    queue_js_file('imagesloaded.min','javascripts/vendor/masonry');
    queue_js_file(array('globals', 'app'));
    echo head_js();
    ?>
</head>
<?php $navLayout = get_theme_option('navigation_layout'); ?>
<?php echo body_tag(array('id' => @$bodyid, 'class' => @$bodyclass . ' ' . $navLayout . '-menu')); ?>
    
<a href="#content" id="skipnav" class="skipnav"><?php echo __('Skip to main content'); ?></a>
    
    <?php fire_plugin_hook('public_body', array('view'=>$this)); ?>
    
    <div id="offCanvas" class="off-canvas position-left" data-off-canvas>
        <?php echo use_foundation_navigation('vertical'); ?>
    </div>
    
    <div class="off-canvas-content" data-off-canvas-content>

    <?php include 'dul_lincoln_masthead.php'; ?>
    
    <div id="content" role="main">
        <div class="grid-container">
        <?php fire_plugin_hook('public_content_top', array('view'=>$this)); ?>
