FROM image-mirror-prod-registry.cloud.duke.edu/library/php:7.4-apache

USER 0

# The OKD4 setup won't allow us to bind to ports 80 or 443, as the 
# containers don't run as "root".
RUN sed -i -r 's/Listen 80/Listen 8080/' /etc/apache2/ports.conf
RUN sed -i -r 's/Listen 443/Listen 8443/' /etc/apache2/ports.conf

# We also need to replace 'VirtualHost *:80'
# Using <VirtualHost _default_:*> will prevent unwanted "redirects"
RUN sed -i -r 's!<VirtualHost \*:80>!<VirtualHost _default_:*>!' /etc/apache2/sites-available/000-default.conf

# Now expose the port(s) we really want to run on
EXPOSE 8080
EXPOSE 8081
EXPOSE 8443


WORKDIR /var/www/html

# Create a default php.ini
COPY ./.omeka_config/php.ini /usr/local/etc/php/php.ini

ARG version=2.7.1
RUN apt-get update && \
    apt-get -y install --no-install-recommends \
    git-core \
    apt-utils \
    unzip \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libmcrypt-dev \
    libpng-dev \
    libmemcached-dev \
    zlib1g-dev \
    exif \
    imagemagick \
    ghostscript
    
#Install php-extensions
RUN docker-php-ext-install -j$(nproc) iconv pdo pdo_mysql gd mysqli exif
RUN docker-php-ext-configure gd --with-freetype=/usr/include/ --with-jpeg=/usr/include/

# ImageMagick
# RUN apt-get install -y libmagickwand-dev --no-install-recommends
# RUN pecl install imagick
# RUN docker-php-ext-enable imagick
# RUN apt-get install -y ghostscript --no-install-recommends

# enable the rewrite module of apache
RUN a2enmod rewrite

# Clone omeka-classic
RUN rm -rf /var/www/html/*
ADD https://github.com/omeka/Omeka/releases/download/v3.1.2/omeka-3.1.2.zip /tmp/omeka-classic.zip
RUN unzip -d /tmp/ /tmp/omeka-classic.zip && mv /tmp/omeka-3.1.2/* /var/www/html/ && rm -rf /tmp/omeka-classic*

# DEBUGGING (turn off for production)
COPY ./.omeka_config/config.ini /var/www/html/application/config/config.ini
RUN chmod 755 /var/www/html/application/config/config.ini

# copy over the database and the apache config
COPY ./.omeka_config/.htaccess /var/www/html/.htaccess
# COPY ./.omeka_config/db.ini /var/www/html/db.ini -- this is now piped in via ConfigMap
COPY ./.omeka_config/apache-config.conf /etc/apache2/sites-enabled/000-default.conf
COPY ./.omeka_config/imagemagick-policy.xml /etc/ImageMagick-6/policy.xml
COPY ./.omeka_config/favicon.ico /var/www/html/favicon.ico

# copy contrib omeka themes
COPY ./themes/ /var/www/html/themes/

# copy contrib plugins
COPY ./plugins/ /var/www/html/plugins/

# copy DUL themes
COPY ./dul_themes/ /var/www/html/themes/

# copy DUL plugins
COPY ./dul_plugins/ /var/www/html/plugins/

# copy customm helpers into exhibit builder plugin
COPY ./dul_themes/omeka-foundation-dul/exhibit-builder/views/helpers/ExhibitAttachmentDUL.php /var/www/html/plugins/ExhibitBuilder/views/helpers/ExhibitAttachmentDUL.php
COPY ./dul_themes/omeka-foundation-dul/exhibit-builder/views/helpers/ExhibitAttachmentGalleryDUL.php /var/www/html/plugins/ExhibitBuilder/views/helpers/ExhibitAttachmentGalleryDUL.php

# copy customm controllers into exhibit builder plugin
COPY ./dul_themes/omeka-foundation-dul/exhibit-builder/controllers/ExhibitsController.php /var/www/html/plugins/ExhibitBuilder/controllers/ExhibitsController.php

# copy customm layouts into exhibit builder plugin
# COPY ./dul_themes/omeka-foundation-dul/exhibit-builder/models/ExhibitLayout.php /var/www/html/plugins/ExhibitBuilder/models/ExhibitLayout.php
# COPY ./dul_themes/omeka-foundation-dul/exhibit-builder/views/shared/exhibit_layouts/lightbox-gallery/ /var/www/html/plugins/ExhibitBuilder/views/shared/exhibit_layouts/lightbox-gallery/

# Copy over kiosk files
COPY ./dul_kiosks/ /var/www/html/kiosks/

# Copy over legacy exhibit files
COPY ./legacy/ /var/www/html/legacy/

# set the file-rights
RUN chown -R www-data:www-data /var/www/html/
RUN chmod -R a+rw /var/www/html/
# RUN chown -R www-data:www-data /var/www/html/files
RUN chmod -R +w /var/www/html/files
VOLUME [ "/var/www/html/files" ]

USER 1001

# Running Apache in foreground
CMD ["apache2-foreground"]
