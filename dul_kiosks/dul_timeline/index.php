<?php

$theID = "1fTz5aXl7VmgdHuQXc093hleRhHGv8gZbH_wuasnUBRM"; // Duke University Timeline Content
$theTitle = 'History of Duke University';

$theURL = "https://sheets.googleapis.com/v4/spreadsheets/" . $theID . "/values/Timeline%20Content?key=AIzaSyCRbmLIwwsgVKWq_0HftFij-6REppY1yFY";

$theJSON = file_get_contents($theURL, 0, $ctx); //the $ctx variable sets a timeout limit
$theData = json_decode($theJSON, TRUE);


?>

<html>

<head>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
  <script src="https://malsup.github.io/jquery.cycle2.js"></script>
  <script src="https://malsup.github.io/jquery.cycle2.center.js"></script>
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,400;0,600;0,800;1,400;1,600;1,800&display=swap" rel="stylesheet">

  <style type="text/css">

    html, body {
		  padding:0;
	  	margin:0;
      width: 1920px;
      height: 1080px;
      overflow: hidden;
	  }

    body {
      background-color: #f0d264;
      background-image: url('gold_background.jpg'); 
    }

    #header {
      width: 100%;
      float: left;
      display: block;
      /* background: #003366; */
      background: #000;
      height: 110px;
      height: 170px;
    }

    #header .left {
      float: left;
    }

    #header h1, h2 {
      margin:0;
      padding:0;
    }

    #header h1 {
      font-family: 'Open Sans', sans-serif;
      color: #fff;
      font-size: 3em;
      font-weight: normal;
      line-height: 2.3em;
      padding-left: 30px;
    }

    #header h2 {
      font-family: 'Open Sans', sans-serif;
      color: #fff;
      font-size: 2em;
      line-height: 3.5em;
      padding-right: 30px;
    }

    #content {
      width: 100%;
      float: left;
      display: block;
      padding-top: 60px;
      height: calc(100% - 130px);
    }

    #content .image-wrapper {
      width: 40%;
      margin-right: 2.5%;
      margin-left: 2.5%;
      float: left;
      text-align: center;
    }

    #content .image-wrapper .image {
      /* max-height: 825px; */
      max-height: 765px;
      max-width: 100%;
      box-shadow: 0 0 20px rgba(0, 0, 0, 0.4);
    }

    #content .image-wrapper .caption {
      font-family: 'Open Sans', sans-serif;
      padding: 0 30px 0 30px;
      font-size: 1.5em;
      color: rgba(0,0,0,.8);
    }

    #content .text-content {
      width: 50%;
      float: left;
      padding: 30px;
      padding-left: 0;
    }

    #content .text-content.full {
      width: 60%;
      margin-left: 20%;
    }

    #content .text-content h2 {
      font-family: 'Open Sans', sans-serif;
      color: #000;
      font-size: 3.5em;
      font-weight: bold;
      line-height: 1.2;
      color: rgba(0,0,0,.8);
    }

    #content .text-content p {
      font-family: 'Open Sans', sans-serif;
      color: #333;
      font-size: 2.5em;
      color: rgba(0,0,0,.8);
    }

    #content .text-content .dates {
      font-family: 'Open Sans', sans-serif;
      font-weight: bold;
      color: rgba(0,0,0,.5);
      margin-bottom: 1em;
      font-size: 3em;
    }

  </style>

</head>

<body>

  <div id="header">
      <div class="left">
          <h1><?php # echo $theTitle ?> &nbsp;</h1>
      </div>
  </div>

  <div id="content">

    <div class='cycle-slideshow'
        data-cycle-speed='1000'
        data-cycle-timeout='20000'
        data-cycle-center-horz='true'
        data-cycle-slides='> div'
        >
    <?php

    // "Start Date" = ['values']['0']
    // "End Date" = ['values']['1']
    // "Headline" = ['values']['2']
    // "Text" = ['values']['3']
    // "Media" = ['values']['4']
    // "Media Credit" = ['values']['5']
    // "Media Caption" = ['values']['6']
    // "Media Thumbnail" = ['values']['7']
    // "Category" = ['values']['8']

    foreach ($theData['values'] as $item) {

      if ($item['0'] == 'Start Date') {
        continue;
      }

    ?>

      <div class="content-wrapper">
        
        <?php if ($item['4'] != '') { ?>

          <div class="image-wrapper">

              <img src="<?php echo $item['4'] ?>" class="image">

              <div class="caption">
                <p><?php echo $item['6'] ?></p>
              </div>

          </div>

        <?php } ?>

        <?php if ( $item['4'] != '' ) { ?>
          <div class="text-content">
        <?php } else { ?>
          <div class="text-content full">
        <?php } ?>


            <div class="dates">

              <?php 

                if (strlen($item['0']) > 4) { 
                  $fromDate = date( 'F j, Y', strtotime($item['0']) );
                } else {
                  $fromDate = $item['0'];
                }

                if (strlen($item['1']) > 4) { 
                  $toDate = date( 'F j, Y', strtotime($item['1']) );
                } else {
                  $toDate = $item['1'];
                }
                
              ?>
              

              <span class="from"><?php echo $fromDate ?></span>
              
              <?php if ( $toDate != "" ) { ?>
                &ndash; <span class="to"><?php echo $toDate ?></span>
              <?php }; ?>
            </div>
            <h2><?php echo $item['2'] ?></h2>
            <p><?php echo $item['3'] ?></p>

        </div>

      </div>

    <?php

    }

    ?>

    </div>

  </div>



<script type="text/javascript">

var IDLE_TIMEOUT = 7200; //seconds
var _idleSecondsCounter = 0;
document.onclick = function() {
    _idleSecondsCounter = 0;
};
document.onmousemove = function() {
    _idleSecondsCounter = 0;
};
document.onkeypress = function() {
    _idleSecondsCounter = 0;
};
window.setInterval(CheckIdleTime, 1000);

function CheckIdleTime() {
    _idleSecondsCounter++;
    var oPanel = document.getElementById("SecondsUntilExpire");
    if (oPanel)
        oPanel.innerHTML = (IDLE_TIMEOUT - _idleSecondsCounter) + "";
    if (_idleSecondsCounter >= IDLE_TIMEOUT) {
        // alert("Time expired!");
        document.location.href = "duke_history_timeline.php";
        // document.location.reload();
    }
}

</script>



</body>

</html>
