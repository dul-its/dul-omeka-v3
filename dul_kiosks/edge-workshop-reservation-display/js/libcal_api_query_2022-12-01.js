(function($) {

  $( document ).ready(function() {

    // get current date/time
    showTheTime();

    // get the room ID
    console.log($('#room').attr("data-room"));

      // 7266 = perkins & bostock
      // 7265 = music library
      // 7264 = lilly library
    
    $theSpace = '7266';
    $theRoom = '';

    // set room conditionally based on roomID

    $roomParam = $('#room').attr("data-room");

    switch ( $roomParam ) {

      case '1': // Project Room 1
        $theRoom = '49578';
        break;
      case '2': // Project Room 2
        $theRoom = '49579';
        break;
      case '3': // Project Room 3
        $theRoom = '49580';
        break;
      case '4': // Project Room 4
        $theRoom = '49581';
        break;
      case '5': // Project Room 5
        $theRoom = '49582';
        break;
      case '6': // Project Room 6
        $theRoom = '49583';
        break;
      case '7': // Project Room 7
        $theRoom = '49584';
        break;
      case '8': // Project Room 8
        $theRoom = '49585';
        break;
      case '9': // Project Room 9
        $theRoom = '49586';
        break;

    }

    console.log('theroom: ' + $theRoom);
    

    // query lobcal -- runs once a minute
    get_availability();
    
  });



// +++ FUNCTIONS +++ //

function showTheTime() {

  var d = new Date($.now());

  $theHour = $.format.date(d, "h:mm a");
  $theDate = $.format.date(d, "MMMM d");

  $('#time').text($theHour);
  $('#date').text($theDate);

  t = setTimeout(function () {
    showTheTime()
  }, 500);

}
  

  // via http://duke.libcal.com/admin/api/1.1/endpoint/space_bookings

  function get_access_token() {
  
    if ( Cookies.get('DUL_scheduler') != null ) {

      if ( Cookies.get('DUL_scheduler') == 'undefined' ) {

        set_access_token();

      } else {

        return Cookies.get('DUL_scheduler');

      }

    } else {

      set_access_token();

    };

  }


  function set_access_token() {
    /* ++ OAuth to Springshare ++ */
    jQuery.ajax( {
      url: 'https://duke.libcal.com/1.1/oauth/token',
      type: 'POST',
      data: { 
        client_id: '853' ,
        client_secret: '787a20d7dc53cf9c8472ac606aef4bec' ,
        grant_type: 'client_credentials' ,
        },
      success: function( response ) {
        access_token = response.access_token;
        var expireTime = 1/24;
        Cookies.set('DUL_scheduler', access_token, {expires: expireTime});
        // console.log('access token: ' + Cookies.get('DUL_scheduler'));
        return Cookies.get('DUL_scheduler');
      }
    } );
  }


  function get_availability() {

    $access_token = get_access_token();

    // availability
    jQuery.ajax( {
      url: 'https://duke.libcal.com/1.1/space/item/' + $theRoom + '?availability',
      type: 'GET',
      beforeSend : function( xhr ) {
        xhr.setRequestHeader( 'Authorization', 'BEARER ' + $access_token );
      },
      data: 'foo',
      success: function( response ) {
        
        $availabilityResponse = response;

        // check for availability
        if ( $availabilityResponse[0]['availability']['length'] > 0 ) {
        
          console.log('availability:');
          console.log($availabilityResponse);
          console.log('+++');

          // check if we are inside a current availability window
          $now = new Date($.now());
          $fromDate = new Date($availabilityResponse[0]['availability'][0]['from']);
          $toDate = new Date($availabilityResponse[0]['availability'][0]['to']);

          if ($now > $fromDate && $now < $toDate) {
            // currently available!
            
            // set display to 'available'
            setAvailable();

            // determine when availability ends
            get_bookings();

          } else {

            // check on bookings
            get_bookings();

          }


        // check for any bookings
        } else {
          
          get_bookings();

        }


        // update grid display

        const timeNumDefaults = ['000', '030', '100', '130', '200', '230', '300', '330', '400', '430', '500', '530', '600', '630', '700', '730', '800', '830', '900', '930', '1000', '1030', '1100', '1130', '1200', '1230', '1300', '1330', '1400', '1430', '1500', '1530', '1600', '1630', '1700', '1730', '1800', '1830', '1900', '1930', '2000', '2030', '2100', '2130', '2200', '2230', '2300', '2330'];
        
        var gridDate = new Date($.now());

        timeNumDefaults.forEach(function(item, index) {
          //console.log('item: ' + item);
          hourCheck = $.format.date(gridDate, "HHmm");
          //console.log('hour: ' + hourCheck);
          if (hourCheck > item) {
            //console.log('greater!');
            $('.time-' + item).removeClass('reserved').removeClass('available');
          } else {
            $('.time-' + item).removeClass('available').addClass('reserved');
            $('.time-' + item).html('<i class="fas fa-times"></i>');
          }
          //console.log('---');
        });

        // align scrollbar
        hourScroll = $.format.date(gridDate, "HH");
        hourScrollNum = (hourScroll / 24) * 1534;

        $('.table-wrap').animate( { scrollLeft: hourScrollNum }, 1000);


        i = 0;
        $availabilityLength = $availabilityResponse[0]['availability']['length'];

        while (i < $availabilityLength) {

          $getStartTime = new Date($availabilityResponse[0]['availability'][i]['from']);

          $startTimeNum = $.format.date($getStartTime, "HHmm");
          
          //console.log('.time-' + $startTimeNum);

          //$('.time-' + startTimeNum).removeClass('reserved');
          $('.time-' + $startTimeNum).removeClass('reserved').addClass('available');
          $('.time-' + $startTimeNum).html('<i class="fas fa-check"></i>');
          //$('.time-' + startTimeNum).html('<i class="far fa-check-circle"></i>');

          i++;

        }

      },
    } );

    // run every 30 seconds
    t = setTimeout(function () {
      get_availability();
    }, 30000);

  }



  function get_bookings() {

    $access_token = get_access_token();

    // bookings
    jQuery.ajax( {
      url: 'https://duke.libcal.com/1.1/space/bookings?lid=' + $theSpace + '&limit=200',
      type: 'GET',
      beforeSend : function( xhr ) {
        xhr.setRequestHeader( 'Authorization', 'BEARER ' + $access_token );
      },
      data: 'foo',
      success: function( response ) {
        
        $bookingsResponse = response;

        console.log('bookings:');
        console.log($bookingsResponse);
        console.log('+++');

        // >> 'status' must be 'confirmed':
        // loop through and find first confirmed instance
        i = 0;
        $bookings_count = $bookingsResponse['length'];
        
        while ($bookings_count > i) {

          if ($bookingsResponse[i]['status'] == 'Confirmed' && $bookingsResponse[i]['eid'] == $theRoom) {

            // check if we are inside a current booking window
            $now = new Date($.now());
            $fromDate = new Date($bookingsResponse[i]['fromDate']);
            $toDate = new Date($bookingsResponse[i]['toDate']);

            // we're in an active booking!
            if ($now > $fromDate && $now < $toDate) {
              
              $fromDateDisplay = $.format.date($fromDate, "h:mm a");
              $toDateDisplay = $.format.date($toDate, "h:mm a");
              setReserved();
              $('.timerange').text( $fromDateDisplay + ' – ' + $toDateDisplay);
              
              $toDateNum = $.format.date($toDate, "HHmm");
              $('.time-' + $toDateNum).removeClass('available');
              $('.time-' + $toDateNum).addClass('reserved');
              console.log('reserved: ' + $toDateNum);
              console.log('+++');

              // got the active booking, break loop
              break;

            } 
            
            // booking is in the future
            if ($now < $fromDate) {
              
              // display time for next booking:
              $fromDateDisplay = $.format.date($fromDate, "h:mm a");
              $('.timerange').text( 'Until ' + $fromDateDisplay);
              
              // got the time, break loop
              break;

            } 
            
            // booking is stale
            if ($now > $toDate) {

              // reset the time display
              $('.timerange').text( '' );
              
              // loop again
              i++;
            
            }

          } else {
            i++;
          }

        }

      },

    } );

  }


  function setAvailable() {

    $('body').removeClass('unavailable').removeClass('reserved').addClass('available');
    $('.room-status h1').text('Available');
    console.log('set as available!');

  }


  function setReserved() {

    $('body').removeClass('unavailable').removeClass('available').addClass('reserved');
    $('.room-status h1').text('Reserved');
    console.log('set as reserved!');

  }


  function setUnavailable() {

    $('body').removeClass('available').removeClass('reserved').addClass('unavailable');
    $('.room-status h1').text('Unavailable');
    console.log('set as unavailable!');

  }


})(jQuery);

