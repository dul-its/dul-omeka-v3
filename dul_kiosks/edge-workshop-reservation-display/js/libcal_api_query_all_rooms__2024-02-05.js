(function($) {

  $( document ).ready(function() {

    // get current date/time
    showTheTime();

    // get the room ID
    // console.log($('#room').attr("data-room"));

      // 7266 = perkins & bostock
      // 7265 = music library
      // 7264 = lilly library
    
    $theSpace = '7266';

    $roomsArray = ['49578', '49579', '49580', '49581', '49582', '49583', '49584', '49585', '49586'];
    

    // update every 60 seconds
    t = setInterval(function () {
      
      console.log('updated');
    
      var i = 0;
      while (i < $roomsArray.length) {
        get_availability($roomsArray[i], i);
        i++;
      }
    
    }, 60000);

  });



// +++ FUNCTIONS +++ //

function showTheTime() {

  var d = new Date($.now());

  $theHour = $.format.date(d, "h:mm a");
  $theDate = $.format.date(d, "MMMM d");

  $('#time').text($theHour);
  $('#date').text($theDate);

  t = setTimeout(function () {
    showTheTime()
  }, 500);

}
  

  // via http://duke.libcal.com/admin/api/1.1/endpoint/space_bookings

  function get_access_token() {
  
    if ( Cookies.get('DUL_scheduler') != null ) {

      if ( Cookies.get('DUL_scheduler') == 'undefined' ) {

        set_access_token();

      } else {

        return Cookies.get('DUL_scheduler');

      }

    } else {

      set_access_token();

    };

  }


  function set_access_token() {
    /* ++ OAuth to Springshare ++ */
    jQuery.ajax( {
      url: 'https://duke.libcal.com/1.1/oauth/token',
      type: 'POST',
      data: { 
        client_id: '853' ,
        client_secret: '787a20d7dc53cf9c8472ac606aef4bec' ,
        grant_type: 'client_credentials' ,
        },
      success: function( response ) {
        access_token = response.access_token;
        var expireTime = 1/24;
        Cookies.set('DUL_scheduler', access_token, {expires: expireTime});
        // console.log('access token: ' + Cookies.get('DUL_scheduler'));
        return Cookies.get('DUL_scheduler');
      }
    } );
  }


  function get_availability(roomID, roomNum) {

    // 0-based array
    roomNum = roomNum + 1;

    $access_token = get_access_token();

    // availability
    jQuery.ajax( {
      url: 'https://duke.libcal.com/1.1/space/item/' + roomID + '?availability',
      type: 'GET',
      beforeSend : function( xhr ) {
        xhr.setRequestHeader( 'Authorization', 'BEARER ' + $access_token );
      },
      data: 'foo',
      success: function( response ) {
        
        $availabilityResponse = response;

        // check for availability
        if ( $availabilityResponse[0]['availability']['length'] > 0 ) {
        
          // console.log('availability:');
          // console.log($availabilityResponse);
          // console.log('+++');

          // check if we are inside a current availability window
          $now = new Date($.now());
          $fromDate = new Date($availabilityResponse[0]['availability'][0]['from']);
          $toDate = new Date($availabilityResponse[0]['availability'][0]['to']);

        } 

        // update grid display

        const timeNumDefaults = ['000', '030', '100', '130', '200', '230', '300', '330', '400', '430', '500', '530', '600', '630', '700', '730', '800', '830', '900', '930', '1000', '1030', '1100', '1130', '1200', '1230', '1300', '1330', '1400', '1430', '1500', '1530', '1600', '1630', '1700', '1730', '1800', '1830', '1900', '1930', '2000', '2030', '2100', '2130', '2200', '2230', '2300', '2330'];
        
        var gridDate = new Date($.now());

        timeNumDefaults.forEach(function(item, index) {
          //console.log('item: ' + item);
          hourCheck = $.format.date(gridDate, "HHmm");
          //console.log('hour: ' + hourCheck);
          if (hourCheck > item) {
            //console.log('greater!');
            $('.room' + roomNum + '--time-' + item).removeClass('reserved').removeClass('available');
          } else {
            $('.room' + roomNum + '--time-' + item).removeClass('available').addClass('reserved');
            $('.room' + roomNum + '--time-' + item).html('<i class="fas fa-times"></i>');
          }
          //console.log('---');
        });

        // align scrollbar
        hourScroll = $.format.date(gridDate, "HH");
        hourScrollNum = ((hourScroll / 24) * 3546) + 5;

        // console.log('hourScrollNum: ' + hourScrollNum);

        $('.table-wrap').animate( { scrollLeft: hourScrollNum }, 1000);


        i = 0;
        $availabilityLength = $availabilityResponse[0]['availability']['length'];

        while (i < $availabilityLength) {

          $getStartTime = new Date($availabilityResponse[0]['availability'][i]['from']);

          $startTimeNum = $.format.date($getStartTime, "HHmm");
          
          //console.log('.time-' + $startTimeNum);

          $('.room' + roomNum + '--time-' + $startTimeNum).removeClass('reserved').addClass('available');
          $('.room' + roomNum + '--time-' + $startTimeNum).html('<i class="fas fa-check"></i>');

          i++;

        }

      },
    } );

  }


})(jQuery);

