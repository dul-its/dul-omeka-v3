<?php 
  // set room name / url based on param

  $theRoomName = 'this room';
  $theURL = 'library.duke.edu/using/room-reservations';
  $theRoom = $_GET["room"];

  switch ($theRoom) {
    case 1:
      $theRoomName = 'Project Room 1';
      $theURL = 'https://duke.is/wd8p8';
      $theQRCode = './qr-codes/project_room_1.png';
      break;
    case 2:
      $theRoomName = 'Project Room 2';
      $theURL = 'https://duke.is/jbbnq';
      $theQRCode = './qr-codes/project_room_2.png';
      break;
    case 3:
      $theRoomName = 'Project Room 3';
      $theURL = 'https://duke.is/yy3jr';
      $theQRCode = './qr-codes/project_room_3.png';
      break;
    case 4:
      $theRoomName = 'Project Room 4';
      $theURL = 'https://duke.is/rkj7j';
      $theQRCode = './qr-codes/project_room_4.png';
      break;
    case 5:
      $theRoomName = 'Project Room 5';
      $theURL = 'https://duke.is/5fdhp';
      $theQRCode = './qr-codes/project_room_5.png';
      break;
    case 6:
      $theRoomName = 'Project Room 6';
      $theURL = 'https://duke.is/64j3f';
      $theQRCode = './qr-codes/project_room_6.png';
      break;
    case 7:
      $theRoomName = 'Project Room 7';
      $theURL = 'https://duke.is/pvdq4';
      $theQRCode = './qr-codes/project_room_7.png';
      break;
    case 8:
      $theRoomName = 'Project Room 8';
      $theURL = 'https://duke.is/4a3w6';
      $theQRCode = './qr-codes/project_room_8.png';
      break;
    case 9:
      $theRoomName = 'Project Room 9';
      $theURL = 'https://duke.is/9x99b';
      $theQRCode = './qr-codes/project_room_9.png';
      break;
  }

?>

<html>

  <head>

    <title>Reserve a room</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta charset="utf-8"/>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="https://kit.fontawesome.com/c5f10ec4a4.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="js/jquery-dateformat.min.js"></script>
    <script type="text/javascript" src="js/js_cookie.js"></script>
    <script type="text/javascript" src="js/libcal_api_query_2022-12-01.js"></script>

    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro&display=swap" rel="stylesheet">

    <link type="text/css" rel="stylesheet" href="css/styles.css" media="all" />

  </head>

  <body class="unavailable">

    <div class="content-wrapper">

      <div class="room-status">

        <h2 class="room-name"><?php print $theRoomName; ?></h2>
      
        <h1 class="status">[offline]</h1>

        <p class="timerange"></p>

      </div>

      <!-- <?php print $theRoom; ?>"><?php print $theRoomName; ?> -->

      <div class="footer">

        <div class="qr-code">    
          <img src="<?php print $theQRCode; ?>" alt="QR Code for <?php print $theRoomName; ?>" />
        </div>

        <div class="reserve-info" id="room" data-room="<?php print $theRoom; ?>">
          <p>To reserve this room, visit:</p>
          <p class="url"><?php print $theURL; ?></p>
        </div>

        <div class="date-time">

          <p class="time" id="time">12:00 am</p>
          <p class="date" id="date">January 1</p>

        </div>
      
      </div>

      <div class="schedule">

        <div class="table-holder">
          
          <div class="table-wrap">

            <table class="table" cellspacing="0" cellpadding="0">
              
              <tr>

                <th colspan="2">
                  12am
                </th>

                <th colspan="2">
                  1am
                </th>

                <th colspan="2">
                  2am
                </th>

                <th colspan="2">
                  3am
                </th>

                <th colspan="2">
                  4am
                </th>

                <th colspan="2">
                  5am
                </th>

                <th colspan="2">
                  6am
                </th>

                <th colspan="2">
                  7am
                </th>

                <th colspan="2">
                  8am
                </th>

                <th colspan="2">
                  9am
                </th>

                <th colspan="2">
                  10am
                </th>

                <th colspan="2">
                  11am
                </th>

                <th colspan="2">
                  12pm
                </th>

                <th colspan="2">
                  1pm
                </th>

                <th colspan="2">
                  2pm
                </th>

                <th colspan="2">
                  3pm
                </th>

                <th colspan="2">
                  4pm
                </th>

                <th colspan="2">
                  5pm
                </th>

                <th colspan="2">
                  6pm
                </th>

                <th colspan="2">
                  7pm
                </th>

                <th colspan="2">
                  8pm
                </th>

                <th colspan="2">
                  9pm
                </th>

                <th colspan="2">
                  10pm
                </th>

                <th colspan="2">
                  11pm
                </th>

              </tr>

              <tr>

                <td class="time-0000">&nbsp;</td>
                <td class="time-0030">&nbsp;</td>

                <td class="time-0100">&nbsp;</td>
                <td class="time-0130">&nbsp;</td>

                <td class="time-0200">&nbsp;</td>
                <td class="time-0230">&nbsp;</td>

                <td class="time-0300">&nbsp;</td>
                <td class="time-0300">&nbsp;</td>

                <td class="time-0400">&nbsp;</td>
                <td class="time-0430">&nbsp;</td>

                <td class="time-0500">&nbsp;</td>
                <td class="time-0530">&nbsp;</td>

                <td class="time-0600">&nbsp;</td>
                <td class="time-0630">&nbsp;</td>

                <td class="time-0700">&nbsp;</td>
                <td class="time-0730">&nbsp;</td>

                <td class="time-0800">&nbsp;</td>
                <td class="time-0830">&nbsp;</td>

                <td class="time-0900">&nbsp;</td>
                <td class="time-0930">&nbsp;</td>

                <td class="time-1000">&nbsp;</td>
                <td class="time-1030">&nbsp;</td>

                <td class="time-1100">&nbsp;</td>
                <td class="time-1130">&nbsp;</td>

                <td class="time-1200">&nbsp;</td>
                <td class="time-1230">&nbsp;</td>

                <td class="time-1300">&nbsp;</td>
                <td class="time-1330">&nbsp;</td>

                <td class="time-1400">&nbsp;</td>
                <td class="time-1430">&nbsp;</td>

                <td class="time-1500">&nbsp;</td>
                <td class="time-1530">&nbsp;</td>

                <td class="time-1600">&nbsp;</td>
                <td class="time-1630">&nbsp;</td>

                <td class="time-1700">&nbsp;</td>
                <td class="time-1730">&nbsp;</td>

                <td class="time-1800">&nbsp;</td>
                <td class="time-1830">&nbsp;</td>

                <td class="time-1900">&nbsp;</td>
                <td class="time-1930">&nbsp;</td>

                <td class="time-2000">&nbsp;</td>
                <td class="time-2030">&nbsp;</td>

                <td class="time-2100">&nbsp;</td>
                <td class="time-2130">&nbsp;</td>

                <td class="time-2200">&nbsp;</td>
                <td class="time-2230">&nbsp;</td>

                <td class="time-2300">&nbsp;</td>
                <td class="time-2330">&nbsp;</td>

              </tr>


            </table>

          </div>

        </div>

      </div>


    </div>

  </body>
</html>
