<?php 


?>

<html>

  <head>

    <title>Reserve a room</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta charset="utf-8"/>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery-dateformat.min.js"></script>
    <script type="text/javascript" src="js/js_cookie.js"></script>
    <script type="text/javascript" src="js/libcal_api_query_all_rooms__2024-02-05.js"></script>

    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro&display=swap" rel="stylesheet">

    <link type="text/css" rel="stylesheet" href="css/all_rooms_styles.css" media="all" />

  </head>

  <body class="unavailable">

    <div class="content-wrapper">

      <!-- SCHEDULE -->
      <div class="schedule">

        <!-- Room Names -->
        <div class="room-names-table-holder">
          <div class="room-names-table-wrap">

            <table class="room-names-table" cellspacing="0" cellpadding="0">

              <tr>
                <th class="room-name">Project Room 1</th>
              </tr>

              <tr>
                <th class="room-name">Project Room 2</th>
              </tr>

              <tr>
                <th class="room-name">Project Room 3</th>
              </tr>

              <tr>
                <th class="room-name">Project Room 4</th>
              </tr>

              <tr>
                <th class="room-name">Project Room 5</th>
              </tr>

              <tr>
                <th class="room-name">Project Room 6</th>
              </tr>

              <tr>
                <th class="room-name">Project Room 7</th>
              </tr>

              <tr>
                <th class="room-name">Project Room 8</th>
              </tr>

              <tr>
                <th class="room-name">Project Room 9</th>
              </tr>
 

            </table>

          </div>
        </div>

        <!-- TIME BLOCKS -->
        <div class="table-holder">
          
          <div class="table-wrap">

            <table class="table times-table" cellspacing="0" cellpadding="0">
              
              <!-- times row -->
              <tr>

                <th colspan="2">
                  12am
                </th>

                <th colspan="2">
                  1am
                </th>

                <th colspan="2">
                  2am
                </th>

                <th colspan="2">
                  3am
                </th>

                <th colspan="2">
                  4am
                </th>

                <th colspan="2">
                  5am
                </th>

                <th colspan="2">
                  6am
                </th>

                <th colspan="2">
                  7am
                </th>

                <th colspan="2">
                  8am
                </th>

                <th colspan="2">
                  9am
                </th>

                <th colspan="2">
                  10am
                </th>

                <th colspan="2">
                  11am
                </th>

                <th colspan="2">
                  12pm
                </th>

                <th colspan="2">
                  1pm
                </th>

                <th colspan="2">
                  2pm
                </th>

                <th colspan="2">
                  3pm
                </th>

                <th colspan="2">
                  4pm
                </th>

                <th colspan="2">
                  5pm
                </th>

                <th colspan="2">
                  6pm
                </th>

                <th colspan="2">
                  7pm
                </th>

                <th colspan="2">
                  8pm
                </th>

                <th colspan="2">
                  9pm
                </th>

                <th colspan="2">
                  10pm
                </th>

                <th colspan="2">
                  11pm
                </th>

              </tr>

            </table>

            <table class="table table-blocks" cellspacing="0" cellpadding="0">

              <!-- Room 1 -->
              <tr class="blocks">
                <td class="room1--time-0000">&nbsp;</td>
                <td class="room1--time-0030">&nbsp;</td>
                <td class="room1--time-0100">&nbsp;</td>
                <td class="room1--time-0130">&nbsp;</td>
                <td class="room1--time-0200">&nbsp;</td>
                <td class="room1--time-0230">&nbsp;</td>
                <td class="room1--time-0300">&nbsp;</td>
                <td class="room1--time-0300">&nbsp;</td>
                <td class="room1--time-0400">&nbsp;</td>
                <td class="room1--time-0430">&nbsp;</td>
                <td class="room1--time-0500">&nbsp;</td>
                <td class="room1--time-0530">&nbsp;</td>
                <td class="room1--time-0600">&nbsp;</td>
                <td class="room1--time-0630">&nbsp;</td>
                <td class="room1--time-0700">&nbsp;</td>
                <td class="room1--time-0730">&nbsp;</td>
                <td class="room1--time-0800">&nbsp;</td>
                <td class="room1--time-0830">&nbsp;</td>
                <td class="room1--time-0900">&nbsp;</td>
                <td class="room1--time-0930">&nbsp;</td>
                <td class="room1--time-1000">&nbsp;</td>
                <td class="room1--time-1030">&nbsp;</td>
                <td class="room1--time-1100">&nbsp;</td>
                <td class="room1--time-1130">&nbsp;</td>
                <td class="room1--time-1200">&nbsp;</td>
                <td class="room1--time-1230">&nbsp;</td>
                <td class="room1--time-1300">&nbsp;</td>
                <td class="room1--time-1330">&nbsp;</td>
                <td class="room1--time-1400">&nbsp;</td>
                <td class="room1--time-1430">&nbsp;</td>
                <td class="room1--time-1500">&nbsp;</td>
                <td class="room1--time-1530">&nbsp;</td>
                <td class="room1--time-1600">&nbsp;</td>
                <td class="room1--time-1630">&nbsp;</td>
                <td class="room1--time-1700">&nbsp;</td>
                <td class="room1--time-1730">&nbsp;</td>
                <td class="room1--time-1800">&nbsp;</td>
                <td class="room1--time-1830">&nbsp;</td>
                <td class="room1--time-1900">&nbsp;</td>
                <td class="room1--time-1930">&nbsp;</td>
                <td class="room1--time-2000">&nbsp;</td>
                <td class="room1--time-2030">&nbsp;</td>
                <td class="room1--time-2100">&nbsp;</td>
                <td class="room1--time-2130">&nbsp;</td>
                <td class="room1--time-2200">&nbsp;</td>
                <td class="room1--time-2230">&nbsp;</td>
                <td class="room1--time-2300">&nbsp;</td>
                <td class="room1--time-2330">&nbsp;</td>
              </tr>

            </table>

            <table class="table table-blocks" cellspacing="0" cellpadding="0">

              <!-- Room 2 -->
              <tr>
                <td class="room2--time-0000">&nbsp;</td>
                <td class="room2--time-0030">&nbsp;</td>
                <td class="room2--time-0100">&nbsp;</td>
                <td class="room2--time-0130">&nbsp;</td>
                <td class="room2--time-0200">&nbsp;</td>
                <td class="room2--time-0230">&nbsp;</td>
                <td class="room2--time-0300">&nbsp;</td>
                <td class="room2--time-0300">&nbsp;</td>
                <td class="room2--time-0400">&nbsp;</td>
                <td class="room2--time-0430">&nbsp;</td>
                <td class="room2--time-0500">&nbsp;</td>
                <td class="room2--time-0530">&nbsp;</td>
                <td class="room2--time-0600">&nbsp;</td>
                <td class="room2--time-0630">&nbsp;</td>
                <td class="room2--time-0700">&nbsp;</td>
                <td class="room2--time-0730">&nbsp;</td>
                <td class="room2--time-0800">&nbsp;</td>
                <td class="room2--time-0830">&nbsp;</td>
                <td class="room2--time-0900">&nbsp;</td>
                <td class="room2--time-0930">&nbsp;</td>
                <td class="room2--time-1000">&nbsp;</td>
                <td class="room2--time-1030">&nbsp;</td>
                <td class="room2--time-1100">&nbsp;</td>
                <td class="room2--time-1130">&nbsp;</td>
                <td class="room2--time-1200">&nbsp;</td>
                <td class="room2--time-1230">&nbsp;</td>
                <td class="room2--time-1300">&nbsp;</td>
                <td class="room2--time-1330">&nbsp;</td>
                <td class="room2--time-1400">&nbsp;</td>
                <td class="room2--time-1430">&nbsp;</td>
                <td class="room2--time-1500">&nbsp;</td>
                <td class="room2--time-1530">&nbsp;</td>
                <td class="room2--time-1600">&nbsp;</td>
                <td class="room2--time-1630">&nbsp;</td>
                <td class="room2--time-1700">&nbsp;</td>
                <td class="room2--time-1730">&nbsp;</td>
                <td class="room2--time-1800">&nbsp;</td>
                <td class="room2--time-1830">&nbsp;</td>
                <td class="room2--time-1900">&nbsp;</td>
                <td class="room2--time-1930">&nbsp;</td>
                <td class="room2--time-2000">&nbsp;</td>
                <td class="room2--time-2030">&nbsp;</td>
                <td class="room2--time-2100">&nbsp;</td>
                <td class="room2--time-2130">&nbsp;</td>
                <td class="room2--time-2200">&nbsp;</td>
                <td class="room2--time-2230">&nbsp;</td>
                <td class="room2--time-2300">&nbsp;</td>
                <td class="room2--time-2330">&nbsp;</td>
              </tr>

            </table>

            <table class="table table-blocks" cellspacing="0" cellpadding="0">

              <!-- Room 3 -->
              <tr>
                <td class="room3--time-0000">&nbsp;</td>
                <td class="room3--time-0030">&nbsp;</td>
                <td class="room3--time-0100">&nbsp;</td>
                <td class="room3--time-0130">&nbsp;</td>
                <td class="room3--time-0200">&nbsp;</td>
                <td class="room3--time-0230">&nbsp;</td>
                <td class="room3--time-0300">&nbsp;</td>
                <td class="room3--time-0300">&nbsp;</td>
                <td class="room3--time-0400">&nbsp;</td>
                <td class="room3--time-0430">&nbsp;</td>
                <td class="room3--time-0500">&nbsp;</td>
                <td class="room3--time-0530">&nbsp;</td>
                <td class="room3--time-0600">&nbsp;</td>
                <td class="room3--time-0630">&nbsp;</td>
                <td class="room3--time-0700">&nbsp;</td>
                <td class="room3--time-0730">&nbsp;</td>
                <td class="room3--time-0800">&nbsp;</td>
                <td class="room3--time-0830">&nbsp;</td>
                <td class="room3--time-0900">&nbsp;</td>
                <td class="room3--time-0930">&nbsp;</td>
                <td class="room3--time-1000">&nbsp;</td>
                <td class="room3--time-1030">&nbsp;</td>
                <td class="room3--time-1100">&nbsp;</td>
                <td class="room3--time-1130">&nbsp;</td>
                <td class="room3--time-1200">&nbsp;</td>
                <td class="room3--time-1230">&nbsp;</td>
                <td class="room3--time-1300">&nbsp;</td>
                <td class="room3--time-1330">&nbsp;</td>
                <td class="room3--time-1400">&nbsp;</td>
                <td class="room3--time-1430">&nbsp;</td>
                <td class="room3--time-1500">&nbsp;</td>
                <td class="room3--time-1530">&nbsp;</td>
                <td class="room3--time-1600">&nbsp;</td>
                <td class="room3--time-1630">&nbsp;</td>
                <td class="room3--time-1700">&nbsp;</td>
                <td class="room3--time-1730">&nbsp;</td>
                <td class="room3--time-1800">&nbsp;</td>
                <td class="room3--time-1830">&nbsp;</td>
                <td class="room3--time-1900">&nbsp;</td>
                <td class="room3--time-1930">&nbsp;</td>
                <td class="room3--time-2000">&nbsp;</td>
                <td class="room3--time-2030">&nbsp;</td>
                <td class="room3--time-2100">&nbsp;</td>
                <td class="room3--time-2130">&nbsp;</td>
                <td class="room3--time-2200">&nbsp;</td>
                <td class="room3--time-2230">&nbsp;</td>
                <td class="room3--time-2300">&nbsp;</td>
                <td class="room3--time-2330">&nbsp;</td>
              </tr>

            </table>

            <table class="table table-blocks" cellspacing="0" cellpadding="0">

              <!-- Room 4 -->
              <tr>
                <td class="room4--time-0000">&nbsp;</td>
                <td class="room4--time-0030">&nbsp;</td>
                <td class="room4--time-0100">&nbsp;</td>
                <td class="room4--time-0130">&nbsp;</td>
                <td class="room4--time-0200">&nbsp;</td>
                <td class="room4--time-0230">&nbsp;</td>
                <td class="room4--time-0300">&nbsp;</td>
                <td class="room4--time-0300">&nbsp;</td>
                <td class="room4--time-0400">&nbsp;</td>
                <td class="room4--time-0430">&nbsp;</td>
                <td class="room4--time-0500">&nbsp;</td>
                <td class="room4--time-0530">&nbsp;</td>
                <td class="room4--time-0600">&nbsp;</td>
                <td class="room4--time-0630">&nbsp;</td>
                <td class="room4--time-0700">&nbsp;</td>
                <td class="room4--time-0730">&nbsp;</td>
                <td class="room4--time-0800">&nbsp;</td>
                <td class="room4--time-0830">&nbsp;</td>
                <td class="room4--time-0900">&nbsp;</td>
                <td class="room4--time-0930">&nbsp;</td>
                <td class="room4--time-1000">&nbsp;</td>
                <td class="room4--time-1030">&nbsp;</td>
                <td class="room4--time-1100">&nbsp;</td>
                <td class="room4--time-1130">&nbsp;</td>
                <td class="room4--time-1200">&nbsp;</td>
                <td class="room4--time-1230">&nbsp;</td>
                <td class="room4--time-1300">&nbsp;</td>
                <td class="room4--time-1330">&nbsp;</td>
                <td class="room4--time-1400">&nbsp;</td>
                <td class="room4--time-1430">&nbsp;</td>
                <td class="room4--time-1500">&nbsp;</td>
                <td class="room4--time-1530">&nbsp;</td>
                <td class="room4--time-1600">&nbsp;</td>
                <td class="room4--time-1630">&nbsp;</td>
                <td class="room4--time-1700">&nbsp;</td>
                <td class="room4--time-1730">&nbsp;</td>
                <td class="room4--time-1800">&nbsp;</td>
                <td class="room4--time-1830">&nbsp;</td>
                <td class="room4--time-1900">&nbsp;</td>
                <td class="room4--time-1930">&nbsp;</td>
                <td class="room4--time-2000">&nbsp;</td>
                <td class="room4--time-2030">&nbsp;</td>
                <td class="room4--time-2100">&nbsp;</td>
                <td class="room4--time-2130">&nbsp;</td>
                <td class="room4--time-2200">&nbsp;</td>
                <td class="room4--time-2230">&nbsp;</td>
                <td class="room4--time-2300">&nbsp;</td>
                <td class="room4--time-2330">&nbsp;</td>
              </tr>

            </table>

            <table class="table table-blocks" cellspacing="0" cellpadding="0">

              <!-- Room 5 -->
              <tr>
                <td class="room5--time-0000">&nbsp;</td>
                <td class="room5--time-0030">&nbsp;</td>
                <td class="room5--time-0100">&nbsp;</td>
                <td class="room5--time-0130">&nbsp;</td>
                <td class="room5--time-0200">&nbsp;</td>
                <td class="room5--time-0230">&nbsp;</td>
                <td class="room5--time-0300">&nbsp;</td>
                <td class="room5--time-0300">&nbsp;</td>
                <td class="room5--time-0400">&nbsp;</td>
                <td class="room5--time-0430">&nbsp;</td>
                <td class="room5--time-0500">&nbsp;</td>
                <td class="room5--time-0530">&nbsp;</td>
                <td class="room5--time-0600">&nbsp;</td>
                <td class="room5--time-0630">&nbsp;</td>
                <td class="room5--time-0700">&nbsp;</td>
                <td class="room5--time-0730">&nbsp;</td>
                <td class="room5--time-0800">&nbsp;</td>
                <td class="room5--time-0830">&nbsp;</td>
                <td class="room5--time-0900">&nbsp;</td>
                <td class="room5--time-0930">&nbsp;</td>
                <td class="room5--time-1000">&nbsp;</td>
                <td class="room5--time-1030">&nbsp;</td>
                <td class="room5--time-1100">&nbsp;</td>
                <td class="room5--time-1130">&nbsp;</td>
                <td class="room5--time-1200">&nbsp;</td>
                <td class="room5--time-1230">&nbsp;</td>
                <td class="room5--time-1300">&nbsp;</td>
                <td class="room5--time-1330">&nbsp;</td>
                <td class="room5--time-1400">&nbsp;</td>
                <td class="room5--time-1430">&nbsp;</td>
                <td class="room5--time-1500">&nbsp;</td>
                <td class="room5--time-1530">&nbsp;</td>
                <td class="room5--time-1600">&nbsp;</td>
                <td class="room5--time-1630">&nbsp;</td>
                <td class="room5--time-1700">&nbsp;</td>
                <td class="room5--time-1730">&nbsp;</td>
                <td class="room5--time-1800">&nbsp;</td>
                <td class="room5--time-1830">&nbsp;</td>
                <td class="room5--time-1900">&nbsp;</td>
                <td class="room5--time-1930">&nbsp;</td>
                <td class="room5--time-2000">&nbsp;</td>
                <td class="room5--time-2030">&nbsp;</td>
                <td class="room5--time-2100">&nbsp;</td>
                <td class="room5--time-2130">&nbsp;</td>
                <td class="room5--time-2200">&nbsp;</td>
                <td class="room5--time-2230">&nbsp;</td>
                <td class="room5--time-2300">&nbsp;</td>
                <td class="room5--time-2330">&nbsp;</td>
              </tr>

            </table>

            <table class="table table-blocks" cellspacing="0" cellpadding="0">

              <!-- Room 6 -->
              <tr>
                <td class="room6--time-0000">&nbsp;</td>
                <td class="room6--time-0030">&nbsp;</td>
                <td class="room6--time-0100">&nbsp;</td>
                <td class="room6--time-0130">&nbsp;</td>
                <td class="room6--time-0200">&nbsp;</td>
                <td class="room6--time-0230">&nbsp;</td>
                <td class="room6--time-0300">&nbsp;</td>
                <td class="room6--time-0300">&nbsp;</td>
                <td class="room6--time-0400">&nbsp;</td>
                <td class="room6--time-0430">&nbsp;</td>
                <td class="room6--time-0500">&nbsp;</td>
                <td class="room6--time-0530">&nbsp;</td>
                <td class="room6--time-0600">&nbsp;</td>
                <td class="room6--time-0630">&nbsp;</td>
                <td class="room6--time-0700">&nbsp;</td>
                <td class="room6--time-0730">&nbsp;</td>
                <td class="room6--time-0800">&nbsp;</td>
                <td class="room6--time-0830">&nbsp;</td>
                <td class="room6--time-0900">&nbsp;</td>
                <td class="room6--time-0930">&nbsp;</td>
                <td class="room6--time-1000">&nbsp;</td>
                <td class="room6--time-1030">&nbsp;</td>
                <td class="room6--time-1100">&nbsp;</td>
                <td class="room6--time-1130">&nbsp;</td>
                <td class="room6--time-1200">&nbsp;</td>
                <td class="room6--time-1230">&nbsp;</td>
                <td class="room6--time-1300">&nbsp;</td>
                <td class="room6--time-1330">&nbsp;</td>
                <td class="room6--time-1400">&nbsp;</td>
                <td class="room6--time-1430">&nbsp;</td>
                <td class="room6--time-1500">&nbsp;</td>
                <td class="room6--time-1530">&nbsp;</td>
                <td class="room6--time-1600">&nbsp;</td>
                <td class="room6--time-1630">&nbsp;</td>
                <td class="room6--time-1700">&nbsp;</td>
                <td class="room6--time-1730">&nbsp;</td>
                <td class="room6--time-1800">&nbsp;</td>
                <td class="room6--time-1830">&nbsp;</td>
                <td class="room6--time-1900">&nbsp;</td>
                <td class="room6--time-1930">&nbsp;</td>
                <td class="room6--time-2000">&nbsp;</td>
                <td class="room6--time-2030">&nbsp;</td>
                <td class="room6--time-2100">&nbsp;</td>
                <td class="room6--time-2130">&nbsp;</td>
                <td class="room6--time-2200">&nbsp;</td>
                <td class="room6--time-2230">&nbsp;</td>
                <td class="room6--time-2300">&nbsp;</td>
                <td class="room6--time-2330">&nbsp;</td>
              </tr>

            </table>

            <table class="table table-blocks" cellspacing="0" cellpadding="0">

              <!-- Room 7 -->
              <tr>
                <td class="room7--time-0000">&nbsp;</td>
                <td class="room7--time-0030">&nbsp;</td>
                <td class="room7--time-0100">&nbsp;</td>
                <td class="room7--time-0130">&nbsp;</td>
                <td class="room7--time-0200">&nbsp;</td>
                <td class="room7--time-0230">&nbsp;</td>
                <td class="room7--time-0300">&nbsp;</td>
                <td class="room7--time-0300">&nbsp;</td>
                <td class="room7--time-0400">&nbsp;</td>
                <td class="room7--time-0430">&nbsp;</td>
                <td class="room7--time-0500">&nbsp;</td>
                <td class="room7--time-0530">&nbsp;</td>
                <td class="room7--time-0600">&nbsp;</td>
                <td class="room7--time-0630">&nbsp;</td>
                <td class="room7--time-0700">&nbsp;</td>
                <td class="room7--time-0730">&nbsp;</td>
                <td class="room7--time-0800">&nbsp;</td>
                <td class="room7--time-0830">&nbsp;</td>
                <td class="room7--time-0900">&nbsp;</td>
                <td class="room7--time-0930">&nbsp;</td>
                <td class="room7--time-1000">&nbsp;</td>
                <td class="room7--time-1030">&nbsp;</td>
                <td class="room7--time-1100">&nbsp;</td>
                <td class="room7--time-1130">&nbsp;</td>
                <td class="room7--time-1200">&nbsp;</td>
                <td class="room7--time-1230">&nbsp;</td>
                <td class="room7--time-1300">&nbsp;</td>
                <td class="room7--time-1330">&nbsp;</td>
                <td class="room7--time-1400">&nbsp;</td>
                <td class="room7--time-1430">&nbsp;</td>
                <td class="room7--time-1500">&nbsp;</td>
                <td class="room7--time-1530">&nbsp;</td>
                <td class="room7--time-1600">&nbsp;</td>
                <td class="room7--time-1630">&nbsp;</td>
                <td class="room7--time-1700">&nbsp;</td>
                <td class="room7--time-1730">&nbsp;</td>
                <td class="room7--time-1800">&nbsp;</td>
                <td class="room7--time-1830">&nbsp;</td>
                <td class="room7--time-1900">&nbsp;</td>
                <td class="room7--time-1930">&nbsp;</td>
                <td class="room7--time-2000">&nbsp;</td>
                <td class="room7--time-2030">&nbsp;</td>
                <td class="room7--time-2100">&nbsp;</td>
                <td class="room7--time-2130">&nbsp;</td>
                <td class="room7--time-2200">&nbsp;</td>
                <td class="room7--time-2230">&nbsp;</td>
                <td class="room7--time-2300">&nbsp;</td>
                <td class="room7--time-2330">&nbsp;</td>
              </tr>

            </table>

            <table class="table table-blocks" cellspacing="0" cellpadding="0">

              <!-- Room 8 -->
              <tr>
                <td class="room8--time-0000">&nbsp;</td>
                <td class="room8--time-0030">&nbsp;</td>
                <td class="room8--time-0100">&nbsp;</td>
                <td class="room8--time-0130">&nbsp;</td>
                <td class="room8--time-0200">&nbsp;</td>
                <td class="room8--time-0230">&nbsp;</td>
                <td class="room8--time-0300">&nbsp;</td>
                <td class="room8--time-0300">&nbsp;</td>
                <td class="room8--time-0400">&nbsp;</td>
                <td class="room8--time-0430">&nbsp;</td>
                <td class="room8--time-0500">&nbsp;</td>
                <td class="room8--time-0530">&nbsp;</td>
                <td class="room8--time-0600">&nbsp;</td>
                <td class="room8--time-0630">&nbsp;</td>
                <td class="room8--time-0700">&nbsp;</td>
                <td class="room8--time-0730">&nbsp;</td>
                <td class="room8--time-0800">&nbsp;</td>
                <td class="room8--time-0830">&nbsp;</td>
                <td class="room8--time-0900">&nbsp;</td>
                <td class="room8--time-0930">&nbsp;</td>
                <td class="room8--time-1000">&nbsp;</td>
                <td class="room8--time-1030">&nbsp;</td>
                <td class="room8--time-1100">&nbsp;</td>
                <td class="room8--time-1130">&nbsp;</td>
                <td class="room8--time-1200">&nbsp;</td>
                <td class="room8--time-1230">&nbsp;</td>
                <td class="room8--time-1300">&nbsp;</td>
                <td class="room8--time-1330">&nbsp;</td>
                <td class="room8--time-1400">&nbsp;</td>
                <td class="room8--time-1430">&nbsp;</td>
                <td class="room8--time-1500">&nbsp;</td>
                <td class="room8--time-1530">&nbsp;</td>
                <td class="room8--time-1600">&nbsp;</td>
                <td class="room8--time-1630">&nbsp;</td>
                <td class="room8--time-1700">&nbsp;</td>
                <td class="room8--time-1730">&nbsp;</td>
                <td class="room8--time-1800">&nbsp;</td>
                <td class="room8--time-1830">&nbsp;</td>
                <td class="room8--time-1900">&nbsp;</td>
                <td class="room8--time-1930">&nbsp;</td>
                <td class="room8--time-2000">&nbsp;</td>
                <td class="room8--time-2030">&nbsp;</td>
                <td class="room8--time-2100">&nbsp;</td>
                <td class="room8--time-2130">&nbsp;</td>
                <td class="room8--time-2200">&nbsp;</td>
                <td class="room8--time-2230">&nbsp;</td>
                <td class="room8--time-2300">&nbsp;</td>
                <td class="room8--time-2330">&nbsp;</td>
              </tr>

            </table>

            <table class="table table-blocks" cellspacing="0" cellpadding="0">

              <!-- Room 9 -->
              <tr>
                <td class="room9--time-0000">&nbsp;</td>
                <td class="room9--time-0030">&nbsp;</td>
                <td class="room9--time-0100">&nbsp;</td>
                <td class="room9--time-0130">&nbsp;</td>
                <td class="room9--time-0200">&nbsp;</td>
                <td class="room9--time-0230">&nbsp;</td>
                <td class="room9--time-0300">&nbsp;</td>
                <td class="room9--time-0300">&nbsp;</td>
                <td class="room9--time-0400">&nbsp;</td>
                <td class="room9--time-0430">&nbsp;</td>
                <td class="room9--time-0500">&nbsp;</td>
                <td class="room9--time-0530">&nbsp;</td>
                <td class="room9--time-0600">&nbsp;</td>
                <td class="room9--time-0630">&nbsp;</td>
                <td class="room9--time-0700">&nbsp;</td>
                <td class="room9--time-0730">&nbsp;</td>
                <td class="room9--time-0800">&nbsp;</td>
                <td class="room9--time-0830">&nbsp;</td>
                <td class="room9--time-0900">&nbsp;</td>
                <td class="room9--time-0930">&nbsp;</td>
                <td class="room9--time-1000">&nbsp;</td>
                <td class="room9--time-1030">&nbsp;</td>
                <td class="room9--time-1100">&nbsp;</td>
                <td class="room9--time-1130">&nbsp;</td>
                <td class="room9--time-1200">&nbsp;</td>
                <td class="room9--time-1230">&nbsp;</td>
                <td class="room9--time-1300">&nbsp;</td>
                <td class="room9--time-1330">&nbsp;</td>
                <td class="room9--time-1400">&nbsp;</td>
                <td class="room9--time-1430">&nbsp;</td>
                <td class="room9--time-1500">&nbsp;</td>
                <td class="room9--time-1530">&nbsp;</td>
                <td class="room9--time-1600">&nbsp;</td>
                <td class="room9--time-1630">&nbsp;</td>
                <td class="room9--time-1700">&nbsp;</td>
                <td class="room9--time-1730">&nbsp;</td>
                <td class="room9--time-1800">&nbsp;</td>
                <td class="room9--time-1830">&nbsp;</td>
                <td class="room9--time-1900">&nbsp;</td>
                <td class="room9--time-1930">&nbsp;</td>
                <td class="room9--time-2000">&nbsp;</td>
                <td class="room9--time-2030">&nbsp;</td>
                <td class="room9--time-2100">&nbsp;</td>
                <td class="room9--time-2130">&nbsp;</td>
                <td class="room9--time-2200">&nbsp;</td>
                <td class="room9--time-2230">&nbsp;</td>
                <td class="room9--time-2300">&nbsp;</td>
                <td class="room9--time-2330">&nbsp;</td>
              </tr>

            </table>

          </div>

        </div>

      </div>


      <!-- INFO -->
      <div class="info">

        <div class="map">    
          <img src="https://library.duke.edu/sites/default/files/dul/kiosks/edge/edge_layout.website.svg" />
        </div>

        <div class="date-time">

          <p class="time" id="time">12:00 am</p>
          <p class="date" id="date">January 1</p>

        </div>

        <div class="reserve-info">
          <p class="url">https://duke.is/the-edge</p>
        </div>

      </div>


    </div>

  </body>
</html>
