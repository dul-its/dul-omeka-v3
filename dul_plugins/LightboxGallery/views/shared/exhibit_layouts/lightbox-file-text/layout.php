<?php
$position = isset($options['file-position'])
    ? html_escape($options['file-position'])
    : 'left';
$size = isset($options['file-size'])
    ? html_escape($options['file-size'])
    : 'fullsize';
$captionPosition = isset($options['captions-position'])
    ? html_escape($options['captions-position'])
    : 'center';
?>
<div class="exhibit-items <?php echo $position; ?> size-<?php echo $size; ?> captions-<?php echo $captionPosition; ?>">
    <?php foreach ($attachments as $attachment): ?>
        <div class="exhibit-item"><?php echo $this->exhibitAttachmentLightbox($attachment, array('imageSize' => $size)); ?></div>
    <?php endforeach; ?>

     <!-- echo item_image_gallery(array('link'=>array('data-lightbox'=>'lightbox'))); -->

</div>
<?php echo $text; ?>
