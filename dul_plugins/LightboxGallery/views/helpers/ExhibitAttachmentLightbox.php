<?php

/**
 * Exhibit attachment view helper.
 * 
 * @package LightboxGallery\View\Helper
 */
class LightboxGallery_View_Helper_ExhibitAttachmentLightbox extends Zend_View_Helper_Abstract
{
    /**
     * Return the markup for displaying an exhibit attachment.
     *
     * @param ExhibitBlockAttachment $attachment
     * @param array $fileOptions Array of options for file_markup
     * @param array $linkProps Array of options for exhibit_builder_link_to_exhibit_item
     * @param boolean $forceImage Whether to display the attachment as an image
     *  always Defaults to false.
     * @return string
     */
    
    
     public function exhibitAttachmentLightbox($attachment, $fileOptions = array(), $linkProps = array(), $forceImage = false, $showTitle = false){
        
        $item = $attachment->getItem();
        $file = $attachment->getFile();
        $caption = $attachment['caption'];
        $caption_text = "";
        $caption_text = $caption_text ? preg_replace('/<span[^>]+\>/i', '', $caption): "";

        // $request = Zend_Controller_Front::getInstance()->getRequest();
        // $exhibitSlug = $request->getParam('slug');
        $exhibitSlug = explode('?', $_SERVER['REQUEST_URI'], 2)[0];
        
        if ($file) {
            // +++ DUL Customizations +++

            // set title to be empty
            $fileOptions['imgAttributes']['title'] = '';

            // get metadata for alt-text and citation
            $typeMetadataArray = all_element_texts($item, array('show_element_sets' => array('Item Type Metadata'), 'return_type' => "array"));
            if (isset($typeMetadataArray['Still Image Item Type Metadata']['Alternative Text'][0])) {
                $itemAltText = $typeMetadataArray['Still Image Item Type Metadata']['Alternative Text'][0];
            };
            if (isset($typeMetadataArray['Still Image Item Type Metadata']['Citation'][0])) {
                $itemCitation = $typeMetadataArray['Still Image Item Type Metadata']['Citation'][0];
            }

            // set title and description vars
            $itemDescription = metadata($item, array('Dublin Core', 'Description'));
            $itemDescription = str_replace(['<br>', '<br />'], '', $itemDescription);
            $itemTitle = metadata($item, array('Dublin Core', 'Title'));

            // account for existing alt text; set title and alt-text to be the same!
            if (isset($itemAltText)) {
                $fileOptions['imgAttributes']['alt'] = $itemAltText;
                $fileOptions['imgAttributes']['title'] = $itemAltText;
            } else {
                $fileOptions['imgAttributes']['alt'] = strip_tags($itemTitle);
                $fileOptions['imgAttributes']['title'] = strip_tags($itemTitle);
            }

            if (!isset($fileOptions['linkAttributes']['data-lightbox'])) {
                $fileOptions['linkAttributes']['data-lightbox'] = 'lightbox-gallery';
            }

            
            if (isset($itemCitation)) {
                // render the citation in the lightbox
                $fileOptions['linkAttributes']['data-title'] = '<p><span class="title">' . $itemTitle . '</span><br />' . $itemCitation . '</p>';
            } else {
                // render the description in the lightbox
                if ($itemDescription != '') {

                    if ( strlen($itemDescription) > 200 ) {
                        $truncatedDescription = substr(strip_tags($itemDescription),0,170).'...';
                        $fileOptions['linkAttributes']['data-title'] = '<p><span class="title">' . $itemTitle . '</span><br />' . $truncatedDescription . '</p>';
                    } else {
                        $fileOptions['linkAttributes']['data-title'] = '<p><span class="title">' . $itemTitle . '</span><br />' . $itemDescription . '</p>';
                    }

                } else {
                    // render only the title in the lightbox
                    $fileOptions['linkAttributes']['data-title'] = '<p><span class="title">' . $itemTitle . '</span></p>';
                }
            }
            
            $fileOptions['linkAttributes']['data-title'] .= '<p class="item-link"><a href="/items/show/' . $item->id . '?slug=' . $exhibitSlug . '">View item page &raquo;</a></p>';

            // +++ END DUL Customizations +++



            if ($forceImage) {
                return 'forceImage!';

                $imageSize = isset($fileOptions['imageSize'])
                    ? $fileOptions['imageSize']
                    : 'square_thumbnail';
                $imageAttr = isset($fileOptions['imgAttributes'])
                    ? $fileOptions['imgAttributes']
                    : array();
                $image = file_image($imageSize, $imageAttr, $file);
                $html = exhibit_builder_link_to_exhibit_item_DUL($image, $linkProps, $item);
            } else {
                // change link to image path
                $fileOptions['linkAttributes']['href'] = file_display_url($file);

                $html = file_markup($file, $fileOptions );
            }
        } else if($item) {
            return 'Item!!!';
            
            $html = exhibit_builder_link_to_exhibit_item_DUL(null, $linkProps, $item);
        }


        // Don't show a caption if we couldn't show the Item or File at all
        if (isset($html)) {
            $captionHtml = $this->view->exhibitAttachmentCaption($attachment);
            if ($showTitle || $captionHtml !== '') {
                $html .= '<div class="slide-meta">';
                if ($showTitle) {
                    $html .= '<p class="slide-title">' . exhibit_builder_link_to_exhibit_item_DUL(null, $linkProps, $item) . '</p>';
                }
                $html .= $captionHtml;
                $html .= '</div>';
            }
        } else {
            $html = '';
        }

        return apply_filters('exhibit_attachment_markup', $html,
            compact('attachment', 'fileOptions', 'linkProps', 'forceImage')
        );
    }

    public function exhibit_builder_link_to_exhibit_item_DUL($text = null, $props = array(), $item = null) {
        if (!$item) {
            $item = get_current_record('item');
        }
    
        if (!isset($props['class'])) {
            $props['class'] = 'exhibit-item-link';
        }
    
        $uri = exhibit_builder_exhibit_item_uri($item);
        $text = (!empty($text) ? $text : strip_formatting(metadata($item, array('Dublin Core', 'Title'))));
        $html = '<a href="' . html_escape($uri) . '" '. tag_attributes($props) . '>' . $text . '</a>';
        $html = apply_filters('exhibit_builder_link_to_exhibit_item', $html, array('text' => $text, 'props' => $props, 'item' => $item));
        // return $html;
        return 'foo!!';
    }

    /**
     * Return the markup for an attachment's caption.
     *
     * @param ExhibitBlockAttachment $attachment
     * @return string
     */
    protected function _caption($attachment)
    {
        if (!is_string($attachment['caption']) || $attachment['caption'] == '') {
            return '';
        }

        $html = '<div class="exhibit-item-caption">'
              . $attachment['caption']
              . '</div>';

        return apply_filters('exhibit_attachment_caption', $html, array(
            'attachment' => $attachment
        ));
    }
}
    
    
    
    
    
    
    
    
    
    
